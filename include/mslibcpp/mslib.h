#ifndef MSLIB_H
#define MSLIB_H

extern "C"{
#include "wzlibc.h"
#include "GL/glew.h"
#include <GL/gl.h>
}

#include <vector>
#include <hash_map>
#include <map>
#include <string>
#include <ctime>

namespace MS{

	void	Init(WZ_Context* ctx);

	class	ImageRegistry;
	class	Zmap;

	class	Object;

	class	Drawable;
	class	Sprite;
	class	Life;
	typedef	Life	Mob;
	class	Character;

	typedef std::pair<int,int>	IntPair;

	struct Property{
		Property*				parent;
		std::vector<Property*>	children;
		WZ_ValUnion				val;
		operator	int					()	const;
		operator	double				()	const;
		operator	std::string			()	const;
		operator	std::pair<int,int>	()	const;
		Property*	operator[]			(int)	const;
	};

	struct PNGPrivateData{
		GLuint			_texture;
		unsigned int	_texSize[2];
	};

	class	ImageRegistry{
	public:
		struct	Image{
			WZ_Image*		image;
			unsigned int	refCount;
			bool			registered;
			Image			(WZ_Image* image);
			~Image			();
			unsigned int	incRef	();
			unsigned int	decRef	();
		};
	protected:
		static	ImageRegistry*	_singleton;
		std::hash_map<WZ_Image*,Image*>		_images;
		ImageRegistry	();
		~ImageRegistry	();
	public:
		static Image&		Get		(WZ_Image* img);
		static void			Release	(Image&	img);
	};
	typedef ImageRegistry::Image Image;

	class	Zmap{
	protected:
		static Zmap*		_singleton;
		static bool			_loaded;
		Zmap	();
		Zmap	(Zmap& zmap);
	public:
		std::map<std::string,Life*>	map;
		~Zmap	();
		static	Zmap*	Dupe	();
		static	void	Load	(ImageRegistry::Image&	img);
	};

	class	Object{
	protected:
		std::string		_name;
	public:
		Object	();
		~Object	();
		const std::string&	name	();
	};

	class	Drawable:virtual public Object{
		friend class Sprite;
	protected:
		std::map<std::string,IntPair>	_map;
		GLuint							_texture;
		int								_origin[2];
		unsigned int					_textureSize[2];
		unsigned int					_size[2];
		unsigned int					_delay;
		ImageRegistry::Image*			_source;
		virtual int						_load	(WZ_SubProperty* sub);
	public:
		Drawable		(WZ_SubProperty* sub);
		~Drawable		();
		virtual void	draw	(int parentPos[2]);
		int				position[2];
		bool			hasMap		(std::string name);
		const IntPair&	operator	[]	(const std::string name);
	};

	class	Sprite:virtual public Drawable{
	protected:
		std::vector<Drawable*>	_frames;
		unsigned int			_currentFrame;
		clock_t					_lastUpdate;
		ImageRegistry::Image*	_source;
		virtual int		_load	(WZ_SubProperty* sub);
	public:
		Sprite			(WZ_SubProperty* sub);
		~Sprite			();
		void			draw	(int parentPos[2]);
		virtual void	update	();
	};

	class	Life:virtual public Sprite{
	protected:
		std::map<std::string,Sprite*>				_states;
		std::map<std::string,Sprite*>::iterator		_currentState;
		int				_load						(WZ_SubProperty* sub);
	public:
		Life			(WZ_SubProperty* sub);
		~Life			();
		void				setState	(const std::string& state);
		void				draw		(int parentPos[2]);
		void				update		();
		const std::string&	getState	();
	};

	class	Character:virtual public Drawable{
	protected:
		std::map<std::string,Zmap*>				_stances;
		std::map<std::string,Zmap*>::iterator	_currentStance;
		Zmap*				_getStance	(const std::string name);
		Life*				_getLife		(const std::string name,const std::string part);
		Life*				_getCurrentLife	(const std::string part);
	public:
		Character			();
		~Character			();
		void	loadImage	(ImageRegistry::Image* Image);
		void	setStance	(const std::string& stance);
		const std::string&	getStance	();
		virtual void	update			();
		virtual void	draw			(int parentPos[2]);
	};

};

#endif