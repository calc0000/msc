#ifndef NXLIBC_H
#define NXLIBC_H

#include <stdarg.h>

#include "ewfutils.h"

struct	NX_Header;
typedef struct NX_Header NX_Header;

typedef enum NX_Type{
	NX_Type_None,
	NX_Type_Int32,
	NX_Type_Double,
	NX_Type_String,
	NX_Type_Vector,
	NX_Type_Canvas,
	NX_Type_MP3,
	NX_Type_Node,
}NX_Type;

typedef union NX_DataUnion{
	unsigned int	intData;
	double			dblData;
	unsigned int	vecData[2];
	unsigned int	strOff;
	unsigned int	canvOff;
	unsigned int	mp3Off;
	unsigned int	nodeOff;
}NX_DataUnion;

typedef struct NX_Data{
	NX_Type			type;
	char			hasOffset;
	unsigned int	offset;
}NX_Data;

typedef struct NX_Primitive{
	NX_Data			inh;
	NX_DataUnion	data;
}NX_Primitive;

typedef struct NX_String{
	NX_Data			inh;
	unsigned short	length;
	unsigned char*	data;
}NX_String;

typedef struct NX_Bitmap{
	NX_Data			inh;
	unsigned short	width;
	unsigned short	height;
	unsigned int	length;
	unsigned int	dataOffset;
	unsigned char*	data;
}NX_Bitmap;

typedef struct NX_MP3{
	NX_Data			inh;
	unsigned int	length;
	unsigned char*	data;
}NX_MP3;

typedef struct NX_Node{
	NX_Header*		header;
	unsigned int	nameIdx;
	NX_Type			type;
	NX_DataUnion	data;
	unsigned short	childrenCount;
	EWF_Array*		children;
}NX_Node;

NX_Node*	NX_Node_Create	(NX_String* name,NX_Data* d,NX_Header* h);
NX_Node*	NX_Node_Get_n	(NX_Node* n,char* name);
NX_Node*	NX_Node_Get_nv	(NX_Node* n,...);

typedef struct NX_Header{
	unsigned char	magic[4];
	unsigned int	nodeCount;
	NX_Node*		root;
	EWF_Array*		strings;
	EWF_Array*		bitmaps;
	EWF_Array*		mp3s;
}NX_Header;

typedef NX_Header NX_File;

NX_File*		NX_File_Open	(EWF_MEMFILE* stream);
NX_Node*		NX_File_Get_nv	(NX_File* f,...);
unsigned int	NX_File_AddData	(NX_File* f,NX_Data* d);
NX_Data*		NX_File_GetData	(NX_File* f,NX_Type t,unsigned int idx);
void			NX_File_Close	(NX_File* f);

#endif