#ifndef __WZ_C_H
#define __WZ_C_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdio.h>

#include "ewfutils.h"

/*some macros for typecasting*/
#define WZ_PRIMITIVEPROPERTY(prop)  ((WZ_PrimitiveProperty*)prop)
#define WZ_PROPERTY(prop)           ((WZ_Property*)prop)
#define WZ_SUBPROPERTY(prop)        ((WZ_SubProperty*)prop)
#define WZ_CANVASPROPERTY(prop)     ((WZ_CanvasProperty*)prop)
#define WZ_OBJECT(prop)             ((WZ_Object*)prop)
#define WZ_SPROPTYPE(prop)          ((WZ_PropertyType)(((int)(WZ_PROPERTY(prop)->propType))/100*100))
#define WZ_FILE(obj)                ((WZ_File*)obj)

typedef enum _enum_WZ_ObjectType {
    WZ_ObjectType_Unknown = 0,
    WZ_ObjectType_File,
    WZ_ObjectType_Header,
    WZ_ObjectType_Image,
    WZ_ObjectType_Directory,
    WZ_ObjectType_Property,
} WZ_ObjectType;

typedef enum _enum_PropertyType {
    WZ_PropertyType_Unknown         = 0,
    WZ_PropertyType_Primitive       = 400,
    WZ_PropertyType_Null                = 401,
    WZ_PropertyType_UnsignedShort       = 402,
    WZ_PropertyType_CompressedInt       = 403,
    WZ_PropertyType_Vector              = 404,
    WZ_PropertyType_Float               = 405,
    WZ_PropertyType_Double              = 406,
    WZ_PropertyType_String              = 407,
    WZ_PropertyType_UOL                 = 408,
    WZ_PropertyType_Sub             = 500,
    WZ_PropertyType_Canvas              = 501,
    WZ_PropertyType_Convex              = 502,
    WZ_PropertyType_PNG             = 700,
    WZ_PropertyType_MP3             = 800,
} WZ_PropertyType;

char*       WZ_PropertyType_GetName(WZ_PropertyType propType);

typedef enum _enum_ErrorCode {
    WZ_Error_NoError = 0,
    WZ_Error_BadFileName,
    WZ_Error_NullParam,
    WZ_Error_BadChain,
    WZ_Error_NoSuchChild,
    WZ_Error_BadOperation,
    WZ_Error_Context_Key_BadLength,
    WZ_Error_File_Parse_BadFileName,
    WZ_Error_Image_Parse_Error1,
    WZ_Error_Directory_Parse_Error1,
    WZ_Error_PNGProp_Parse_BadFormat,
} WZ_ErrorCode;

const char* WZ_GetError();
char*       WZ_GetError_copy();

struct _struct_WZ_Context;
typedef struct _struct_WZ_Context   WZ_Context;

struct _struct_WZ_Object;
typedef struct _struct_WZ_Object    WZ_Object;
struct _struct_WZ_Directory;
typedef struct _struct_WZ_Directory WZ_Directory;
struct _struct_WZ_File;
typedef struct _struct_WZ_File      WZ_File;
struct _struct_WZ_Header;
typedef struct _struct_WZ_Header    WZ_Header;
struct _struct_WZ_Image;
typedef struct _struct_WZ_Image     WZ_Image;

struct _struct_WZ_Property;
typedef struct _struct_WZ_Property          WZ_Property;
struct _struct_WZ_PrimitiveProperty;
typedef struct _struct_WZ_PrimitiveProperty WZ_PrimitiveProperty;
struct _struct_WZ_SubProperty;
typedef struct _struct_WZ_SubProperty       WZ_SubProperty;
struct _struct_WZ_CanvasProperty;
typedef struct _struct_WZ_CanvasProperty    WZ_CanvasProperty;
struct _struct_WZ_PNGProperty;
typedef struct _struct_WZ_PNGProperty       WZ_PNGProperty;
struct _struct_WZ_SoundProperty;
typedef struct _struct_WZ_SoundProperty     WZ_SoundProperty;

struct _struct_WZ_Context {
    void*           (*calloc)   (size_t num, size_t size);
    void*           (*realloc)  (void* mem, size_t size);
    void            (*free)     (void* mem);
    unsigned char*  key;
    void*           renderPNGUserData;
    int             (*renderPNG)(WZ_Context*, WZ_PNGProperty*, void*);
    void*           unparsePNGUserData;
    int             (*unparsePNG)(WZ_Context*, WZ_PNGProperty*, void*);
    void*           renderMP3UserData;
    int             (*renderMP3)(WZ_Context*, WZ_SoundProperty*, void*);
    void*           unparseMP3UserData;
    int             (*unparseMP3)(WZ_Context*, WZ_SoundProperty*, void*);
    void*           progressUserData;
    int             (*progress)(WZ_Context*, double, void*);
};
WZ_Context*     WZ_Context_New              ();
void            WZ_Context_SetDefaults      (WZ_Context* ctx);
WZ_ErrorCode    WZ_Context_LoadKeyFromFile  (WZ_Context* ctx, MEMFILE* stream);
WZ_ErrorCode    WZ_Context_SetKey           (WZ_Context* ctx, unsigned char* buffer, unsigned int length);
void            WZ_Context_SetCurrent       (WZ_Context* ctx);
WZ_Context*     WZ_Context_GetCurrent       ();

void            WZ_Context_Free             (WZ_Context* ctx);

struct _struct_WZ_Object {
    WZ_Context*     context;
    WZ_Object*      parent;
    char*           name;
    WZ_ObjectType   type;
    int             parsed;
    int             blockSize;
    int             checksum;
    unsigned int    offset;
    unsigned int    objectID;
    unsigned int    refCount;
};

char*       WZ_ObjectType_GetName   (WZ_ObjectType type);
int         WZ_Object_GetSize       (WZ_Object* obj);
WZ_Object*  WZ_Object_Get_n         (WZ_Object* obj, const char* name);
WZ_Object*  WZ_Object_Get_nv        (WZ_Object* obj, ...);
WZ_Object*  WZ_Object_Get_i         (WZ_Object* obj, int index);
WZ_Object*  WZ_Object_Get_iv        (WZ_Object* obj, ...);

struct _struct_WZ_Header {
    WZ_Object           inh;
    char*               ident;
    unsigned long long  fileSize;
    unsigned int        fileStart;
    short               version;
    char*               copyright;
    unsigned int        versionHash;
};

struct _struct_WZ_File {
    WZ_Object       inh;
    int             _fileVersion;
    MEMFILE*        _stream;
    WZ_Directory*   _wzDir;
    const char*     _fileName;
    WZ_Header*      _header;
};

WZ_File*        WZ_File_Open        (WZ_Context*, MEMFILE* stream, const char* name);
void            WZ_File_Close       (WZ_File* file);
WZ_ErrorCode    WZ_File_Parse       (WZ_File* file);
WZ_Object*      WZ_File_Get_n       (WZ_File* file, const char* name);
WZ_Object*      WZ_File_Get_i       (WZ_File* file, int i);
int             WZ_File_GetSize     (WZ_File* file);

struct _struct_WZ_Directory {
    WZ_Object       inh;
    WZ_File*        parentFile;
    EWF_LL*         _firstChild;
};

WZ_Object*      WZ_Directory_Get_n      (WZ_Directory* dir, const char* name);
WZ_Object*      WZ_Directory_Get_i      (WZ_Directory* dir, int i);
int             WZ_Directory_GetSize    (WZ_Directory* dir);

struct _struct_WZ_Image {
    WZ_Object       inh;
    WZ_File*        parentFile;
    WZ_SubProperty* _sub;
    MEMFILE*        _stream;
};

WZ_ErrorCode    WZ_Image_Parse          (WZ_Image* image);
void            WZ_Image_Unparse        (WZ_Image* image);
WZ_Property*    WZ_Image_Get_n          (WZ_Image* image, const char* name);
WZ_Property*    WZ_Image_Get_i          (WZ_Image* image, int i);
int             WZ_Image_GetSize        (WZ_Image* image);
WZ_ErrorCode    WZ_Image_ResolveUOLs    (WZ_Image* image);

struct _struct_WZ_Property {
    WZ_Object       inh;
    WZ_Image*       parentImg;
    WZ_PropertyType propType;
    int             uolD;
};

/* deep copy */
WZ_Property* WZ_Property_Copy(WZ_Property* prop);

typedef union _union_WZ_ValUnion {
    char*   strVal;
    double  dblVal;
    int     intVal;
    int     vecVal[2];
    void*   nullVal;
} WZ_ValUnion;

struct _struct_WZ_PrimitiveProperty {
    WZ_Property inh;
    WZ_ValUnion val;
};

WZ_Property*    WZ_UOLProperty_Resolve  (WZ_PrimitiveProperty* uolProp);

struct _struct_WZ_SubProperty {
    WZ_Property inh;
    EWF_LL* _firstChild;
};

typedef WZ_SubProperty WZ_ConvexProperty;

WZ_Property*    WZ_SubProperty_Get_n        (WZ_SubProperty* subProp, const char* name);
WZ_Property*    WZ_SubProperty_Get_i        (WZ_SubProperty* subProp, int i);
int             WZ_SubProperty_GetSize      (WZ_SubProperty* subProp);
WZ_ErrorCode    WZ_SubProperty_ResolveUOLs  (WZ_SubProperty* subProp);

struct _struct_WZ_PNGProperty {
    WZ_Property     inh;
    int             pngParsed;
    int             width;
    int             height;
    int             _format;
    int             _format2;
    int             _length;
    unsigned char*  _compBytes;
    int             _decLen;
    unsigned char*  _decBytes;
    unsigned char*  _pixels;
    unsigned int    _pixelsLen;
    MEMFILE*        _stream;
    void*           userData;
};

WZ_ErrorCode    WZ_PNGProperty_Parse    (WZ_PNGProperty* pngProp);
void            WZ_PNGProperty_Unparse  (WZ_PNGProperty* pngProp);

typedef struct _struct_WZ_CanvasProperty {
    WZ_SubProperty  inh;
    WZ_PNGProperty* png;
} WZ_CanvasProperty;

struct _struct_WZ_SoundProperty {
    WZ_Property     inh;
    int             _lenData;
    int             lenMS;
    unsigned char*  _data;
    void*           userData;
};

#ifdef __cplusplus
}
#endif

#endif
