local NAME="msc"
local INCLIB_ROOT= "inclib/"
local EWF_LIBS={"ewfthreads","ewfutils"}
local GL_LIBS={"glew32","opengl32","glu32"}
local SDL_LIBS={"SDL","SDLmain","SDL_mixer"}
local ZLIB_LIBS={"zlib"}
local LZ4_LIBS={"lz4"}
local other_libs={"cairo"}
local ALL_LIBS={(EWF_LIBS),(GL_LIBS),(SDL_LIBS),(ZLIB_LIBS),(other_libs)}

solution (NAME)

	location "./proj"
	configurations {"Debug","Release"}
	includedirs {"include"}
	
	project("wz2nx")
		kind "ConsoleApp"
		language "C"
		files {"src/wz2nx/*.c"}
		
		includedirs{
			(INCLIB_ROOT),
			("./include/wzlibc/"),
			("./include/nxlibc/"),
		}
		libdirs{
			(INCLIB_ROOT),
		}
		
		configuration "Debug"
			defines { "DEBUG", "_DEBUG", "_STLP_DEBUG" }
			flags { "Symbols" }
			targetdir "bin/debug"
			objdir "obj/debug"
			links {"nxlibc","wzlibc",(ZLIB_LIBS),(EWF_LIBS),(LZ4_LIBS)}
		
		configuration "Release"
			defines { "NDEBUG", "_NDEBUG" }
			flags { "OptimizeSpeed" }
			targetdir "bin/release"
			objdir "obj/release"
			links {"nxlibc","wzlibc",(ZLIB_LIBS),(EWF_LIBS),(LZ4_LIBS)}
		
		configuration {"windows"}
			defines {"WIN32","_WIN32","_WINDOWS"}
	
	project("nxlibc")
		kind "StaticLib"
		language "C"
		files {"src/nxlibc/*.c","include/nxlibc/*.h"}
		
		includedirs{
			(INCLIB_ROOT),
			("include/nxlibc/"),
		}
		libdirs{
			(INCLIB_ROOT),
		}
		
		configuration "Debug"
			defines { "DEBUG", "_DEBUG", "_STLP_DEBUG" }
			flags { "Symbols" }
			targetdir "bin/debug"
			objdir "obj/debug"
			links {}
		
		configuration "Release"
			defines { "NDEBUG", "_NDEBUG" }
			flags { "OptimizeSpeed" }
			targetdir "bin/release"
			objdir "obj/release"
			links {}
		
		configuration {"windows"}
			defines {"WIN32","_WIN32","_WINDOWS"}
	
	project("mslib_test")
		kind "ConsoleApp"
		language "C++"
		files {"src/mslib_test/*.cpp","include/mslib_test/*.h"}
		
		includedirs{
			(INCLIB_ROOT),
			("./include/wzlibc/"),
			("./include/mslibcpp/"),
		}
		libdirs{
			(INCLIB_ROOT),
		}
		
		configuration "Debug"
			defines { "DEBUG", "_DEBUG", "_STLP_DEBUG" }
			flags { "Symbols" }
			targetdir "bin/debug"
			objdir "obj/debug"
			links {"mslibcpp","wzlibc",(GL_LIBS),(SDL_LIBS),(ZLIB_LIBS),(EWF_LIBS)}
		
		configuration "Release"
			defines { "NDEBUG", "_NDEBUG" }
			flags { "OptimizeSpeed" }
			targetdir "bin/release"
			objdir "obj/release"
			links {"mslibcpp","wzlibc",(GL_LIBS),(SDL_LIBS),(ZLIB_LIBS),(EWF_LIBS)}
		
		configuration {"windows"}
			defines {"WIN32","_WIN32","_WINDOWS"}
			
	project("wzlibc_bench")
		kind "ConsoleApp"
		language "C"
		files {"src/wzlibc_bench/*.c"}
		
		includedirs{
			(INCLIB_ROOT),
			("./include/wzlibc/"),
		}
		libdirs{
			(INCLIB_ROOT),
		}
		
		configuration "Debug"
			defines { "DEBUG", "_DEBUG", "_STLP_DEBUG" }
			flags { "Symbols" }
			targetdir "bin/debug"
			objdir "obj/debug"
			links {"wzlibc",(SDL_LIBS),(EWF_LIBS),(ZLIB_LIBS)}
		
		configuration "Release"
			defines { "NDEBUG", "_NDEBUG" }
			flags { "OptimizeSpeed" }
			targetdir "bin/release"
			objdir "obj/release"
			links {"wzlibc",(SDL_LIBS),(EWF_LIBS),(ZLIB_LIBS)}
		
		configuration {"windows"}
			defines {"WIN32","_WIN32","_WINDOWS"}
	
	project("wz")
		kind "ConsoleApp"
		language "C"
		files {"src/wz/*.c"}
		
		includedirs{
			(INCLIB_ROOT),
			("./include/wzlibc/"),
		}
		libdirs{
			(INCLIB_ROOT),
		}
		
		configuration "Debug"
			defines { "DEBUG", "_DEBUG", "_STLP_DEBUG" }
			flags { "Symbols" }
			targetdir "bin/debug"
			objdir "obj/debug"
			links {"wzlibc",(GL_LIBS),(SDL_LIBS),(ZLIB_LIBS),(EWF_LIBS)}
		
		configuration "Release"
			defines { "NDEBUG", "_NDEBUG" }
			flags { "OptimizeSpeed" }
			targetdir "bin/release"
			objdir "obj/release"
			links {"wzlibc",(GL_LIBS),(SDL_LIBS),(ZLIB_LIBS),(EWF_LIBS)}
		
		configuration {"windows"}
			defines {"WIN32","_WIN32","_WINDOWS"}
	
	project("wz_opengl")
		kind "ConsoleApp"
		language "C"
		files {"src/wz_opengl/*.c","include/wz_opengl/*.h"}
		
		includedirs{
			(INCLIB_ROOT),
			("./include/wzlibc/"),
			("./include/mslibc/"),
		}
		libdirs{
			(INCLIB_ROOT),
		}
		
		configuration "Debug"
			defines { "DEBUG", "_DEBUG", "_STLP_DEBUG" }
			flags { "Symbols" }
			targetdir "bin/debug"
			objdir "obj/debug"
			links {"mslibc","wzlibc",(GL_LIBS),(SDL_LIBS),(ZLIB_LIBS)}
		
		configuration "Release"
			defines { "NDEBUG", "_NDEBUG" }
			flags { "OptimizeSpeed" }
			targetdir "bin/release"
			objdir "obj/release"
			links {"mslibc","wzlibc",(GL_LIBS),(SDL_LIBS),(ZLIB_LIBS)}
		
		configuration {"windows"}
			defines {"WIN32","_WIN32","_WINDOWS"}
	
	project("mslibcpp")
		kind "StaticLib"
		language "C++"
		files {"src/mslibcpp/*.cpp","include/mslibcpp/*.h"}
		
		includedirs{
			(INCLIB_ROOT),
			("./include/wzlibc/"),
			("./include/mslibcpp/"),
		}
		libdirs{
			(INCLIB_ROOT),
		}
		
		configuration "Debug"
			defines { "DEBUG", "_DEBUG", "_STLP_DEBUG" }
			flags { "Symbols" }
			targetdir "bin/debug"
			objdir "obj/debug"
			links {}
		
		configuration "Release"
			defines { "NDEBUG", "_NDEBUG" }
			flags { "OptimizeSpeed" }
			targetdir "bin/release"
			objdir "obj/release"
			links {}
		
		configuration {"windows"}
			defines {"WIN32","_WIN32","_WINDOWS"}
	
	project("mslibc")
		kind "StaticLib"
		language "C"
		files {"src/mslibc/*.c","include/mslibc/*.h"}
		
		includedirs{
			(INCLIB_ROOT),
			("./include/wzlibc/"),
			("./include/mslibc/"),
		}
		libdirs{
			(INCLIB_ROOT),
		}
		
		configuration "Debug"
			defines { "DEBUG", "_DEBUG", "_STLP_DEBUG" }
			flags { "Symbols" }
			targetdir "bin/debug"
			objdir "obj/debug"
			links {"wzlibc",(ALL_LIBS)}
		
		configuration "Release"
			defines { "NDEBUG", "_NDEBUG" }
			flags { "OptimizeSpeed" }
			targetdir "bin/release"
			objdir "obj/release"
			links {"wzlibc",(ALL_LIBS)}
		
		configuration {"windows"}
			defines {"WIN32","_WIN32","_WINDOWS"}
	
	project("wzlibc")
		kind "StaticLib"
		language "C"
		files {"src/wzlibc/*.c","include/wzlibc/*.h"}
		
		includedirs{
			(INCLIB_ROOT),
			("./include/wzlibc/"),
		}
		libdirs{
			(INCLIB_ROOT),
		}
		
		configuration "Debug"
			defines { "DEBUG", "_DEBUG", "_STLP_DEBUG" }
			flags { "Symbols" }
			targetdir "bin/debug"
			objdir "obj/debug"
			links {(ZLIB_LIBS)}
		
		configuration "Release"
			defines { "NDEBUG", "_NDEBUG" }
			flags { "OptimizeSpeed" }
			targetdir "bin/release"
			objdir "obj/release"
			links {(ZLIB_LIBS)}
		
		configuration {"windows"}
			defines {"WIN32","_WIN32","_WINDOWS"}