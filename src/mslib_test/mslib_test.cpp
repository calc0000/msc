#include "wzlibc.h"
#include "mslib.h"

#include "SDL/SDL.h"

int		initGraphics		(unsigned int width,unsigned int height,unsigned int bpp);
void	deInitGraphics		();

int main(int argc,char* argv[]){
	WZ_Context ctx={NULL};
	ctx.calloc=calloc;
	ctx.realloc=realloc;
	ctx.free=free;
	MS::Init(&ctx);
	initGraphics(800,600,32);

	WZ_File* dataWZ=WZ_File_Open(&ctx,mlink(mnew(),fopen("wz40b/Data.wz","rb")),"Data.wz");
	WZ_File_Parse(dataWZ);
	MS::Image& img=MS::ImageRegistry::Get((WZ_Image*)WZ_Object_Get_nv((WZ_Object*)dataWZ,"Skill","500.img",NULL));
	WZ_SubProperty* wzS=(WZ_SubProperty*)WZ_Object_Get_nv((WZ_Object*)img.image,"skill","5001006","effect",NULL);
	MS::Sprite* spr=new MS::Sprite(wzS);
	spr->position[0]=200;
	spr->position[1]=200;

	img=MS::ImageRegistry::Get((WZ_Image*)WZ_Object_Get_nv((WZ_Object*)dataWZ,"Mob","3230303.img",NULL));
	MS::Mob* mob=new MS::Mob(img.image->_sub);
	mob->position[0]=400;
	mob->position[1]=400;
	mob->setState("die1");

	glClearColor(1,0,1,1);
	glClearDepth(1.0f);
	glViewport(0,0,800,600);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,800,600,0,1,-1);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_TEXTURE_2D);
	glLoadIdentity();
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	SDL_Event ev;
	bool running=true;
	while(running){
		while(SDL_PollEvent(&ev)){
			switch(ev.type){
			case SDL_QUIT:
				running=false;break;
			case SDL_KEYUP:
				if(ev.key.keysym.sym==SDLK_ESCAPE)
					running=false;
				break;
			}
		}
		{//draw/update here
			glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

			spr->update();
			spr->draw(NULL);

			mob->update();
			mob->draw(NULL);

			SDL_GL_SwapBuffers();
			SDL_Delay(1);
		}
	}

	SDL_Quit();
	return 0;
}

int		initGraphics		(unsigned int width,unsigned int height,unsigned int bpp){
	if(SDL_Init(SDL_INIT_EVERYTHING)<0)
		return -1;

	if(SDL_SetVideoMode(width,height,bpp,SDL_HWSURFACE|SDL_GL_DOUBLEBUFFER|SDL_OPENGL)==NULL)
		return -1;

	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE,16);

	glClearColor(1,0,1,1);
	glClearDepth(1.0f);
	glViewport(0,0,width,height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,width,height,0,1,-1);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_TEXTURE_2D);
	glLoadIdentity();
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);//2D*/

	/*glClearColor(0,0,0,.5);
	glClearDepth(1.0);
	glViewport(0,0,width,height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45,((double)width)/((double)height),0.1,100);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glEnable(GL_TEXTURE_2D);
	//glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glShadeModel(GL_SMOOTH);
	//glCullFace(GL_BACK);
	//glFrontFace(GL_CCW);
	//glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT,GL_NICEST);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glEnable(GL_POINT_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
	glEnable(GL_POLYGON_SMOOTH);
	glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);//3D*/

	return 0;
}
void	deInitGraphics		(){
	SDL_Quit();
}