#include "mslib.h"

namespace MS{

	Property::operator int() const{
		return val.intVal;
	}
	Property::operator double() const{
		return val.dblVal;
	}
	Property::operator std::string() const{
		return std::string(val.strVal);
	}
	Property::operator std::pair<int,int>() const{
		return std::make_pair<int,int>(val.vecVal[0],val.vecVal[1]);
	}
	Property*	Property::operator [](int i) const{
		return children[i];
	}

	int		renderPNG	(WZ_Context* ctx,WZ_PNGProperty* png,void* data){
		GLuint tex=0;
		PNGPrivateData* d=NULL;
		glGenTextures(1,&tex);
		glBindTexture(GL_TEXTURE_2D,tex);
		glTexImage2D(GL_TEXTURE_2D,0,4,png->width,png->height,0,GL_RGBA,GL_UNSIGNED_BYTE,png->_pixels);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
		d=new PNGPrivateData();
		d->_texture=tex;
		d->_texSize[0]=png->width;
		d->_texSize[1]=png->height;
		png->userData=d;
		return 0;
	}
	int		unparsePNG	(WZ_Context* ctx,WZ_PNGProperty* png,void* data){
		PNGPrivateData* d=(PNGPrivateData*)png->userData;
		if(d!=NULL){
			glDeleteTextures(1,&(d->_texture));
		}
		delete d;
		png->userData=NULL;
		return 0;
	}

	void	Init(WZ_Context* ctx){
		glewInit();
		ctx->renderPNG=renderPNG;
		ctx->unparsePNG=unparsePNG;
	}

	Zmap*	Zmap::_singleton=NULL;
	bool	Zmap::_loaded=false;
	Zmap::Zmap():map(){
	}
	Zmap::Zmap(Zmap& zmap){
		map=zmap.map;
		for(auto i=map.begin(),l=map.end();i!=l;i++)
			i->second=NULL;
	}
	Zmap::~Zmap(){
	}
	Zmap*	Zmap::Dupe(){
		if(Zmap::_singleton==NULL)
			Zmap::_singleton=new Zmap();
		Zmap* ret=new Zmap(*Zmap::_singleton);
		return ret;
	}
	void	Zmap::Load(ImageRegistry::Image& img){
		if(Zmap::_loaded)
			return;
		if(Zmap::_singleton==NULL)
			Zmap::_singleton=new Zmap();
		img.incRef();
		for(unsigned int i=0,l=WZ_Image_GetSize(img.image);i<l;i++){
			WZ_Property* prop=WZ_Image_Get_i(img.image,i);
			Zmap::_singleton->map[WZ_PRIMITIVEPROPERTY(prop)->val.strVal]=NULL;
		}
		img.decRef();
		Zmap::_loaded=true;
	}

	Object::Object():_name(){
	}
	Object::~Object(){
	}
	const std::string& Object::name(){
		return _name;
	}

};