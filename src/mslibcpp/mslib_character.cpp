#include "mslib.h"

namespace MS{

	struct	CharacterSprite:virtual public Sprite{
	protected:
		Zmap*	_zmap;
		int		_load		(WZ_SubProperty* sub);
	public:
		CharacterSprite		(WZ_SubProperty* sub);
		~CharacterSprite	();
		std::map<std::string,std::pair<int,int>>	maps;
	};

	struct	CharacterLife:virtual public Life{
	protected:
		int		_load		(WZ_SubProperty* sub);
	public:
		CharacterLife		(WZ_SubProperty* sub);
		~CharacterLife		();
	};
	int	CharacterLife::_load(WZ_SubProperty* sub){
		if(sub==NULL)
			return 0;
		return 0;
	}
	CharacterLife::CharacterLife(WZ_SubProperty* sub):Life(sub),Drawable(NULL),Sprite(NULL){
	}
	CharacterLife::~CharacterLife(){
	}

	Character::Character():_stances(),Drawable(NULL){
	}
	Character::~Character(){
		for(auto i=_stances.begin(),l=_stances.end();i!=l;i++){
			Zmap* zmap=i->second;
			for(auto i2=zmap->map.begin(),l2=zmap->map.end();i2!=l2;i2++)
				delete i2->second;
		}
	}

};