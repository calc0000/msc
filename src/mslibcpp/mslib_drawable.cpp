#include "mslib.h"

namespace MS{

	Drawable::Drawable(WZ_SubProperty* sub):Object(),_map(){
		_source=NULL;
		_load(sub);
		position[0]=position[1]=0;
	}
	Drawable::~Drawable(){
		ImageRegistry::Release(*_source);
	}
	int		Drawable::_load(WZ_SubProperty* sub){
		if(sub==NULL)
			return 0;
		if(WZ_PROPERTY(sub)->propType==WZ_PropertyType_Canvas){
			//assume mob style 
			WZ_PNGProperty* png=NULL;
			png=WZ_CANVASPROPERTY(sub)->png;
			if(png!=NULL){
				if(!png->pngParsed)
					WZ_PNGProperty_Parse(png);
				_texture=((PNGPrivateData*)png->userData)->_texture;
				_textureSize[0]=_size[0]=((PNGPrivateData*)png->userData)->_texSize[0];
				_textureSize[1]=_size[1]=((PNGPrivateData*)png->userData)->_texSize[1];
				_source=&ImageRegistry::Get(WZ_PROPERTY(png)->parentImg);
				{
					WZ_PrimitiveProperty* pp;
					pp=(WZ_PrimitiveProperty*)WZ_Object_Get_n((WZ_Object*)sub,"origin");
					if(pp!=NULL){
						_origin[0]=pp->val.vecVal[0];
						_origin[1]=pp->val.vecVal[1];
					}else{
						_origin[0]=0;
						_origin[1]=0;
					}
					pp=(WZ_PrimitiveProperty*)WZ_Object_Get_n((WZ_Object*)sub,"delay");
					if(pp!=NULL)
						_delay=pp->val.intVal;
					else
						_delay=100;
				}
				{
					static char* maps[]={"head","lt","rb"};
					WZ_PrimitiveProperty* pp=NULL;
					for(unsigned int i=0,l=sizeof(maps)/sizeof(char*);i<l;i++){
						pp=(WZ_PrimitiveProperty*)WZ_Object_Get_n((WZ_Object*)sub,maps[i]);
						if(pp!=NULL && WZ_PROPERTY(pp)->propType==WZ_PropertyType_Vector){
							IntPair& newP=_map[std::string(maps[i])];
							newP.first=pp->val.vecVal[0];
							newP.second=pp->val.vecVal[1];
						}
					}
				}
			}
		}else if(WZ_PROPERTY(sub)->propType==WZ_PropertyType_Sub){
			//assume char style
		}
		return 0;
	}
	bool	Drawable::hasMap(std::string name){
		return _map.find(name)==_map.end();
	}
	const IntPair&	Drawable::operator	[]	(const std::string name){
		return _map[name];
	}
	void	Drawable::draw(int parentPos[2]){
		int drawX=0,drawY=0;
		if(parentPos!=NULL){
			drawX=position[0]-_origin[0]+parentPos[0];
			drawY=position[1]-_origin[1]+parentPos[1];
		}else{
			drawX=position[0]-_origin[0];
			drawY=position[1]-_origin[1];
		}
		glBindTexture(GL_TEXTURE_2D,_texture);

		//glColor4f(0,1,0,0.5);
		glBegin(GL_QUADS);
			glTexCoord2i(0,0);
			glVertex2i(drawX,drawY);
			glTexCoord2i(1,0);
			glVertex2i(drawX+_size[0],drawY);
			glTexCoord2i(1,1);
			glVertex2i(drawX+_size[0],drawY+_size[1]);
			glTexCoord2i(0,1);
			glVertex2i(drawX,drawY+_size[1]);
		glEnd();
	}

};