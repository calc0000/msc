#include "mslib.h"

namespace MS{

	Life::Life(WZ_SubProperty* sub):_states(),Sprite(NULL),Drawable(NULL){
		if(sub!=NULL){
			_load(sub);
			_source=&ImageRegistry::Get(WZ_PROPERTY(sub)->parentImg);
		}
	}
	Life::~Life(){
		for(auto i=_states.begin(),l=_states.end();i!=l;i++){
			delete i->second;
		}
		ImageRegistry::Release(*_source);
	}
	int Life::_load(WZ_SubProperty* sub){
		for(unsigned int i=0,l=WZ_SubProperty_GetSize(sub);i<l;i++){
			WZ_Property* prop=(WZ_Property*)WZ_SubProperty_Get_i(sub,i);
			std::string name=WZ_OBJECT(prop)->name;
			if(name!="info" && name!="icon"){
				Sprite* spr=new Sprite((WZ_SubProperty*)prop);
				_states[name]=spr;
			}
		}
		return 0;
	}
	void	Life::setState(const std::string& state){
		auto it=_states.find(state);
		if(it==_states.end()){
			_currentState=_states.begin();
			return;
		}
		_currentState=it;
	}
	const std::string& Life::getState	(){
		return _currentState->first;
	}
	void	Life::draw(int parentPos[2]){
		int drawPos[2]={position[0],position[1]};
		if(parentPos!=NULL){
			drawPos[0]+=parentPos[0];
			drawPos[1]+=parentPos[1];
		}
		_currentState->second->draw(drawPos);
	}
	void	Life::update(){
		_currentState->second->update();
	}

};