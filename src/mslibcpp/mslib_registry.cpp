#include "mslib.h"

namespace MS{

	ImageRegistry::Image::Image(WZ_Image* image){
		this->image=image;
		this->refCount=0;
	}
	ImageRegistry::Image::~Image(){
		while(this->refCount>0)
			decRef();
	}
	unsigned int	ImageRegistry::Image::incRef(){
		if(this->refCount==0 && image!=NULL){
			WZ_Image_Parse(image);
			WZ_Image_ResolveUOLs(image);
		}
		this->refCount++;
		return this->refCount;
	}
	unsigned int	ImageRegistry::Image::decRef(){
		if(this->refCount==0)
			return 0;
		this->refCount--;
		if(this->refCount==0 && image!=NULL)
			WZ_Image_Unparse(image);
		return this->refCount;
	}

	ImageRegistry*	ImageRegistry::_singleton=NULL;
	ImageRegistry::ImageRegistry():_images(){
	}
	ImageRegistry::~ImageRegistry(){
		for(auto i=_images.begin(),l=_images.end();i!=l;i++){
			delete i->second;
		}
	}
	ImageRegistry::Image&	ImageRegistry::Get(WZ_Image* img){
		if(ImageRegistry::_singleton==NULL)
			ImageRegistry::_singleton=new ImageRegistry();
		auto it=ImageRegistry::_singleton->_images.find(img);
		if(it==ImageRegistry::_singleton->_images.end()){
			ImageRegistry::Image* newI=new ImageRegistry::Image(img);
			ImageRegistry::_singleton->_images[img]=newI;
			newI->incRef();
			return *newI;
		}else{
			it->second->incRef();
			return *(it->second);
		}
	}
	void	ImageRegistry::Release(ImageRegistry::Image& img){
		if(ImageRegistry::_singleton==NULL)
			ImageRegistry::_singleton=new ImageRegistry();
		img.decRef();
	}

};