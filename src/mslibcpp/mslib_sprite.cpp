#include "mslib.h"

namespace MS{

	Sprite::Sprite(WZ_SubProperty* sub):_frames(),Drawable(NULL){
		if(sub!=NULL){
			_load(sub);
			_source=&ImageRegistry::Get(WZ_PROPERTY(sub)->parentImg);
		}
		_currentFrame=0;
		_lastUpdate=0;
	}
	Sprite::~Sprite(){
		for(auto i=_frames.begin(),l=_frames.end();i!=l;i++){
			delete *i;
		}
		ImageRegistry::Release(*_source);
	}
	int Sprite::_load(WZ_SubProperty* sub){
		if(sub==NULL)
			return 0;
		for(unsigned int i=0,l=WZ_SubProperty_GetSize(sub);i<l;i++){
			WZ_Property* prop=WZ_SubProperty_Get_i(sub,i);
			WZ_CanvasProperty* cProp;
			WZ_PNGProperty* pProp;
			Drawable* newD=NULL;
			if(prop->propType!=WZ_PropertyType_Canvas)
				continue;
			cProp=(WZ_CanvasProperty*)prop;
			pProp=cProp->png;
			newD=new Drawable((WZ_SubProperty*)cProp);
			_frames.push_back(newD);
		}
		if(_frames.size()>0)
			_currentFrame=0;
		return 0;
	}
	void	Sprite::draw(int parentPos[2]){
		int drawPos[2]={position[0],position[1]};
		if(_frames.size()<=0)
			return;
		if(parentPos!=NULL){
			drawPos[0]+=parentPos[0];
			drawPos[1]+=parentPos[1];
		}
		_frames[_currentFrame]->draw(drawPos);
	}
	void	Sprite::update(){
		clock_t now=clock()*1000/CLOCKS_PER_SEC;
		if(_frames.size()<=0)
			return;
		if(_frames[_currentFrame]->_delay<=(now-_lastUpdate)){
			_currentFrame++;
			if(_currentFrame>=_frames.size())
				_currentFrame=0;
			_lastUpdate=now;
		}
	}

};