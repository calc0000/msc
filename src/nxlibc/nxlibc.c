#include "nxlibc.h"

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

NX_Node*	NX_Node_Create	(NX_String* name,NX_Data* d,NX_Header* h){
	NX_Node* ret=(NX_Node*)calloc(1,sizeof(NX_Node));
	unsigned int nameIdx=NX_File_AddData(h,(NX_Data*)name);
	ret->type=d->type;
	switch(ret->type){
	case NX_Type_Int32:
	case NX_Type_Double:
	case NX_Type_Vector:
		ret->data=((NX_Primitive*)d)->data;
		break;
	default:
		{
			unsigned int dataIdx=NX_File_AddData(h,d);
			ret->data.strOff=dataIdx;
		}
		break;
	}
	ret->header=h;
	return ret;
}
NX_Node*	NX_Node_Get_n	(NX_Node* n,char* name){
	if(n && n->children && name){
		unsigned short i=0;
		for(;i<n->childrenCount;i++){
			NX_Node* c=(NX_Node*)*EWF_Array_Index(n->children,i);
			NX_String* nameStr=(NX_String*)NX_File_GetData(n->header,NX_Type_String,c->nameIdx);
			if(strncmp((const char*)nameStr->data,name,nameStr->length)==0)
				return c;
		}
	}
	return NULL;
}
NX_Node*	NX_Node_Get_nv	(NX_Node* n,...){
	va_list ap;
	NX_Node* curNode=n;
	char* curStr=NULL;
	va_start(ap,n);
	curStr=va_arg(ap,char*);
	while(curNode && curStr){
		curNode=NX_Node_Get_n(curNode,curStr);
		curStr=va_arg(ap,char*);
	}
	va_end(ap);
	return curNode;
}

NX_Data*	NX_File_GetData	(NX_File* f,NX_Type t,unsigned int idx){
	if(f){
		EWF_Array* arr=NULL;
		switch(t){
		case NX_Type_String:
			arr=f->strings;break;
		case NX_Type_Canvas:
			arr=f->bitmaps;break;
		case NX_Type_MP3:
			arr=f->mp3s;break;
		}
		if(arr)
			return (NX_Data*)*EWF_Array_Index(arr,idx);
	}
	return NULL;
}
unsigned int	NX_File_AddData	(NX_File* f,NX_Data* d){
	switch(d->type){
	case NX_Type_None:
	case NX_Type_Int32:
	case NX_Type_Double:
	case NX_Type_Vector:
		return 0;
	case NX_Type_String:
		{
		}break;
	case NX_Type_Canvas:
		{
		}break;
	case NX_Type_MP3:
		{
		}break;
	default:
		return 0;
	}
}