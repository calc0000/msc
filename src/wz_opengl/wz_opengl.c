#define WZLIB_HAVE_OPENGL
#include "wzlibc.h"
#include "mslibc.h"

#include <math.h>
#include <time.h>

#include "SDL/SDL.h"
//#include "dynthread.h"

#define TRUE 1==1
#define FALSE 1!=1

/*int main(int argc,char* argv[]){
	int running=TRUE;
	int i=0;
	SDL_Event ev;
	SDL_Surface* screen=NULL;

	WZLib_File* wzF=NULL;
	WZLib_Image* wzI=NULL;
	WZLib_PNGProperty* wzPNG=NULL;
	WZLib_PNGProperty* wzPNG2=NULL;
	WZLib_CanvasProperty* wzC=NULL;

	SDL_Init(SDL_INIT_EVERYTHING);
	screen=SDL_SetVideoMode(800,600,32,SDL_HWSURFACE|SDL_GL_DOUBLEBUFFER|SDL_OPENGL);
	if(screen==NULL)
		printf("!!screen NULL\n");

	//init wzlib
	//Data.wz->Skill->410.img->skill->4101003->icon
	//Map.wz -> Back -> midForest -> back -> 0.png
	//Data.wz -> Skill -> 310.img -> skill -> 3101005 -> hit -> 0 -> 4.png
	WZLib_Key_LoadKeyFromFile("wz40b/bms.key");
	wzF=WZLib_File_Open("wz40b/Data.wz");
	WZLib_File_Parse(wzF);
	wzI=(WZLib_Image*)WZLib_Object_Get_nv((WZLib_Object*)wzF,"Skill","310.img",NULL);
	WZLib_Image_Parse(wzI);
	wzC=(WZLib_CanvasProperty*)WZLib_Object_Get_nv((WZLib_Object*)wzI,"skill","3101005","hit","0","3",NULL);
	if(wzC==NULL){
		printf("!!wzC is NULL\n");
		system("pause");
		return 0;
	}
	wzPNG=wzC->png;
	wzI=(WZLib_Image*)WZLib_Object_Get_nv((WZLib_Object*)wzF,"Skill","500.img",NULL);
	WZLib_Image_Parse(wzI);
	wzC=(WZLib_CanvasProperty*)WZLib_Object_Get_nv((WZLib_Object*)wzI,"skill","5001006","effect","1",NULL);
	wzPNG2=wzC->png;
	if(wzPNG==NULL){
		printf("!!wzPNG is NULL\n");
		system("pause");
		return 0;
	}
	printf("wzPNG : format %d, width %d, height %d, texID %d, texWidth %d, texHeight %d\n",wzPNG->_format+wzPNG->_format2,wzPNG->width,wzPNG->height,wzPNG->texture,wzPNG->texWidth,wzPNG->texHeight);
	printf("wzPNG2: format %d, width %d, height %d, texID %d, texWidth %d, texHeight %d\n",wzPNG2->_format+wzPNG2->_format2,wzPNG2->width,wzPNG2->height,wzPNG2->texture,wzPNG2->texWidth,wzPNG2->texHeight);
	WZLib_PNGProperty_Parse(wzPNG);
	WZLib_PNGProperty_Parse(wzPNG2);

	glClearColor(0,0,0,0);
	glClearDepth(1.0f);
	glViewport(0,0,800,600);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,800,600,0,1,-1);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_TEXTURE_2D);
	glLoadIdentity();
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	glTranslatef(350,250,0);
	glClearColor(1.0,1.0,1.0,0);
	i=0;
	while(running){
		while(SDL_PollEvent(&ev)){
			switch(ev.type){
			case SDL_QUIT:
				running=FALSE;break;
			case SDL_KEYDOWN:
				if(ev.key.keysym.sym==SDLK_ESCAPE)
					running=FALSE;
				break;
			}
		}
		{
			float wr,hr;
			wr=((float)wzPNG->width)/((float)wzPNG->texWidth);
			hr=((float)wzPNG->height)/((float)wzPNG->texHeight);
			glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
			glBindTexture(GL_TEXTURE_2D,wzPNG->texture);

			glLoadIdentity();
			glTranslatef(350,250,0);

			glColor4f(1,1,1,1);

			glBegin(GL_QUADS);
				glTexCoord2f(0,0);
				glVertex3f(0,0,0);
				glTexCoord2f(wr,0);
				glVertex3f(wzPNG->width,0,0);
				glTexCoord2f(wr,hr);
				glVertex3f(wzPNG->width,wzPNG->height,0);
				glTexCoord2f(0,hr);
				glVertex3f(0,wzPNG->height,0);
			glEnd();
			
			glTranslatef(100,100,0);
			glBindTexture(GL_TEXTURE_2D,wzPNG2->texture);
			wr=((float)wzPNG2->width)/((float)wzPNG2->texWidth);
			hr=((float)wzPNG2->height)/((float)wzPNG2->texHeight);
			glBegin(GL_QUADS);
				glTexCoord2f(0,0);
				glVertex3f(0,0,0);
				glTexCoord2f(wr,0);
				glVertex3f(wzPNG2->width,0,0);
				glTexCoord2f(wr,hr);
				glVertex3f(wzPNG2->width,wzPNG2->height,0);
				glTexCoord2f(0,hr);
				glVertex3f(0,wzPNG2->height,0);
			glEnd();

			SDL_GL_SwapBuffers();
		}
		SDL_Delay(1);
	}

	SDL_Quit();
	return 0;
}//*/

int main(int argc,char* argv[]){
	int running=TRUE;
	int i=0;
	SDL_Event ev;
	SDL_Surface* screen=NULL;

	WZLib_File* wzF=NULL;
	WZLib_Image* wzI=NULL;
	WZLib_SubProperty* wzS=NULL;

	MS_Sprite* spr=MS_Sprite_New();
	MS_Mob* mob=NULL;

	SDL_Init(SDL_INIT_EVERYTHING);
	MS_Init(SDL_GetTicks);
	screen=SDL_SetVideoMode(800,600,32,SDL_HWSURFACE|SDL_GL_DOUBLEBUFFER|SDL_OPENGL);
	if(screen==NULL)
		printf("!!screen NULL\n");
	//Mob -> 3230303.img
	WZLib_Key_LoadKeyFromFile("wz40b/bms.key");
	wzF=WZLib_File_Open("wz40b/Data.wz");
	{
		unsigned int tix=SDL_GetTicks();
		WZLib_File_Parse(wzF);
		printf("Took %d ticks to parse.\n",SDL_GetTicks()-tix);
	}
	wzI=(WZLib_Image*)WZLib_Object_Get_nv((WZLib_Object*)wzF,"Skill","500.img",NULL);
	WZLib_Image_Parse(wzI);
	wzS=(WZLib_SubProperty*)WZLib_Object_Get_nv((WZLib_Object*)wzI,"skill","5001006","effect",NULL);
	printf("Found %d sprite children.\n",MS_Sprite_LoadFromSubProperty(spr,wzS));
	spr->inh.position[0]=spr->inh.position[1]=200;

	wzI=(WZLib_Image*)WZLib_Object_Get_nv((WZLib_Object*)wzF,"Mob","3230303.img",NULL);
	mob=MS_Mob_LoadFromImage(wzI);
	mob->inh.inh.inh.position[0]=mob->inh.inh.inh.position[1]=400;
	MS_Life_ChangeCurrentState((MS_Life*)mob,"die1");
	printf("Current mob state: %s\n",MS_Life_GetCurrentStateName((MS_Life*)mob));

	MS_Character_New();

	glClearColor(0,0,0,0);
	glClearDepth(1.0f);
	glViewport(0,0,800,600);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0,800,600,0,1,-1);
	glMatrixMode(GL_MODELVIEW);
	glEnable(GL_TEXTURE_2D);
	glLoadIdentity();
	glDepthMask(GL_FALSE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	//glTranslatef(350,250,0);
	glClearColor(1.0,0.0,1.0,1);
	i=0;
	while(running){
		while(SDL_PollEvent(&ev)){
			switch(ev.type){
			case SDL_QUIT:
				running=FALSE;break;
			case SDL_KEYDOWN:
				if(ev.key.keysym.sym==SDLK_ESCAPE)
					running=FALSE;
				break;
			}
		}
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		/*glBindTexture(GL_TEXTURE_2D,0);
		glColor4f(0,0,0,.5);
		glBegin(GL_QUADS);
			glVertex2f(spr->inh.position[0],spr->inh.position[1]);
			glVertex2f(spr->inh.position[0],mob->inh.inh.inh.position[1]);
			glVertex2f(mob->inh.inh.inh.position[0],mob->inh.inh.inh.position[1]);
			glVertex2f(mob->inh.inh.inh.position[0],spr->inh.position[1]);
		glEnd();
		glColor4f(1,1,1,1);//*/

		glBindTexture(GL_TEXTURE_2D,0);
		glColor4f(0,.5,0,1);
		glBegin(GL_QUADS);
		glVertex2f(100,550);
		glVertex2f(150,550);
		glVertex2f(150,500);
		glVertex2f(100,500);
		glEnd();
		glColor4f(1,1,1,1);//*/

		MS_Sprite_Update(spr);
		MS_Sprite_Draw(spr,NULL);
		MS_Mob_Update(mob);
		MS_Mob_Draw(mob,NULL);

		SDL_GL_SwapBuffers();
		SDL_Delay(1);
	}

	SDL_Quit();
	return 0;
}