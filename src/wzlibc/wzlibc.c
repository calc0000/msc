#include "wzlibc.h"

#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <stdio.h>
#include <stdarg.h>

#include "zlib.h"

#ifdef WIN32
#pragma warning(disable:4996)
#endif

#define WZPP(prop) WZ_PRIMITIVEPROPERTY(prop)

unsigned int    __WZ_OBJECT_ID_COUNTER = 0;
char*           __WZ_LastError = "";
unsigned char   __WZ_DefaultKey[65536] = {0};
WZ_Context      __WZ_DefaultContext = {
    calloc,
    realloc,
    free,
    __WZ_DefaultKey,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
};
WZ_Context*     __WZ_CurrentContext = &__WZ_DefaultContext;


#pragma region This File Declarations
/*Functions priv to this MEMFILE*/
static  WZ_ErrorCode        _WZ_Error_SetAndReturn      (WZ_ErrorCode err, char* format, ...);
WZ_ErrorCode                _WZ_Object_Init             (WZ_Context* ctx, WZ_Object* obj, const char* name, WZ_ObjectType type);
WZ_ErrorCode                _WZ_Directory_Init          (WZ_Context* ctx, WZ_Directory* dir, const char* name);
void                        _WZ_Directory_Free          (WZ_Directory* dir);
WZ_ErrorCode                _WZ_Directory_Parse         (WZ_Directory* dir, MEMFILE* stream, WZ_Header* header);
WZ_ErrorCode                _WZ_Image_Init              (WZ_Context* ctx, WZ_Image* img, const char* name);
WZ_ErrorCode                _WZ_Header_Init             (WZ_Context* ctx, WZ_Header* header, const char* name);
WZ_ErrorCode                _WZ_Header_Parse            (WZ_Header* header, MEMFILE* stream);
WZ_ErrorCode                _WZ_Property_Init           (WZ_Context* ctx, WZ_Property* ret, const char* name, WZ_PropertyType propType);
WZ_ErrorCode                _WZ_SubProperty_Init        (WZ_Context* ctx, WZ_SubProperty* ret, const char* name);
WZ_ErrorCode                _WZ_SubProperty_Parse       (WZ_SubProperty* sub, MEMFILE* stream, unsigned int offset);
EWF_LL*                     _WZ_SubProperty_Add         (WZ_SubProperty* sub, WZ_Property* prop);
void                        _WZ_SubProperty_Free        (WZ_SubProperty* sub);
WZ_SubProperty*             _WZ_SubProperty_Copy        (WZ_SubProperty* subProp);
WZ_ErrorCode                _WZ_PrimitiveProperty_Init  (WZ_Context* ctx, WZ_PrimitiveProperty* ret, const char* name, WZ_PropertyType propType);
WZ_ErrorCode                _WZ_CanvasProperty_Init     (WZ_Context* ctx, WZ_CanvasProperty* ret, const char* name);
void                        _WZ_CanvasProperty_Free     (WZ_CanvasProperty* canv);
WZ_ErrorCode                _WZ_PNGProperty_Init        (WZ_Context* ctx, WZ_PNGProperty* ret, const char* name);
WZ_PNGProperty*             _WZ_PNGProperty_Copy        (WZ_PNGProperty* pngProp);
WZ_ErrorCode                _WZ_PNGProperty_Read        (WZ_PNGProperty* png, MEMFILE* stream);
/*void                      _WZ_PNGProperty_Free        (WZ_PNGProperty* png);*/
WZ_ErrorCode                _WZ_SoundProperty_Init      (WZ_Context* ctx, WZ_SoundProperty* ret, const char* name);
void                        _WZ_SoundProperty_Parse     (WZ_SoundProperty* prop, MEMFILE* stream);
void                        _WZ_SoundProperty_Free      (WZ_SoundProperty* prop);
WZ_Property*                _WZ_ExtendedProperty_Parse  (WZ_Context* ctx, MEMFILE* stream, const char* name, int offset, int eob, WZ_Image* parentImg);
unsigned int                _WZ_File_GetVersionHash     (int encver, int realver);
/*---------------------------*/
#pragma endregion

#pragma region Utilities
void _WZ_tokenize(const char* str, const char* delim, char*** outTokens, int* numTokens) {
    const int guessSize = 2;
    char** tokens = (char**)__WZ_CurrentContext->calloc(guessSize, sizeof(char*));
    int index = 0;
    int tokenStart = 0;
    int tokenIndex = 0;
    int tokensCap = guessSize;
    int len = strlen(str);
    int delimLen = strlen(delim);
    for(index = 0; index <= len; index++) {
        if(strncmp(str + sizeof(char)*index, delim, delimLen) == 0) { /*match using strcmp*/
            int strlen = 0;
            if(index == tokenStart) {
                tokenStart++; /*in case it's two delims after the other*/
                continue;
            }
            /*do we need to expand tokens?*/
            if(tokenIndex + 1 >= tokensCap) {
                tokens = (char**)__WZ_CurrentContext->realloc(tokens, sizeof(char*) * (tokensCap + guessSize));
                tokensCap += guessSize;
            }
            strlen = index - tokenStart;
            tokens[tokenIndex] = (char*)__WZ_CurrentContext->calloc((strlen + 1), sizeof(char));
            memcpy(tokens[tokenIndex], str + (sizeof(char)*tokenStart), strlen + 1);
            tokens[tokenIndex][strlen] = 0; /*null terminate pl0x*/
            tokenIndex++;
            tokenStart = index + 1;
        }
    }
    if(str[tokenStart] != 0) {
        int strlen = 0;
        if(tokenIndex + 1 >= tokensCap) {
            tokens = (char**)__WZ_CurrentContext->realloc(tokens, sizeof(char*) * (tokensCap + guessSize));
            tokensCap += guessSize;
        }
        strlen = index - tokenStart;
        tokens[tokenIndex] = (char*)__WZ_CurrentContext->calloc((strlen + 1), sizeof(char));
        memcpy(tokens[tokenIndex], str + (sizeof(char)*tokenStart), strlen + 1);
        tokens[tokenIndex][strlen] = 0; /*null terminate pl0x*/
        tokenIndex++;
        tokenStart = index + 1;
    }
    if(tokenIndex < tokensCap)
        tokens = (char**)__WZ_CurrentContext->realloc(tokens, sizeof(char*)*tokenIndex);
    *outTokens = tokens;
    *numTokens = tokenIndex;
}
int _WZ_z_decompress_known(unsigned char* inBuffer, unsigned int inLen, unsigned char** outBufferPtr, unsigned int outLen) {
    const unsigned int inChunk = 4096;
    unsigned int inBufferIndex = 0;
    const unsigned int outChunk = 4096;
    unsigned int outBufferIndex = 0;
    unsigned char* outBuffer = (unsigned char*)__WZ_CurrentContext->calloc(outLen, sizeof(unsigned char));
    int err = Z_OK;
    z_stream strm;
    {
        strm.next_in = NULL;
        strm.avail_in = 0;
        strm.opaque = NULL;
        strm.zfree = Z_NULL;
        strm.zalloc = Z_NULL;
        inflateInit(&strm);

        do {
            unsigned int old_avail_out = 0;
            strm.next_in = inBuffer + sizeof(unsigned char) * inBufferIndex;
            strm.avail_in = (inLen - inBufferIndex > inChunk ? inChunk : inLen - inBufferIndex);
            inBufferIndex += strm.avail_in;
            if(strm.avail_in == 0)
                break;
            strm.next_out = outBuffer + outBufferIndex;
            strm.avail_out = outLen - outBufferIndex;
            old_avail_out = strm.avail_out;
            err = inflate(&strm, Z_NO_FLUSH);
            outBufferIndex += old_avail_out - strm.avail_out;
            switch(err) {
            case Z_NEED_DICT:
            case Z_DATA_ERROR:
            case Z_MEM_ERROR:
            case Z_BUF_ERROR:
                inflateEnd(&strm);
                return err;
            }
        } while(err != Z_STREAM_END);

        inflateEnd(&strm);
    }
    *outBufferPtr = outBuffer;
    return Z_OK;
}
#pragma endregion

#pragma region File Reading Functions
/*File Reading Functions*/
#define M_Read_Primitive(stream,type,val)   mread(&val,sizeof(type),1,stream)
char* M_Read_NullString(MEMFILE* stream) {
    unsigned int stringStart = 0;
    unsigned int stringLength = 0;
    char c;
    char* ret = NULL;
    stringStart = mtell(stream);
    do {
        M_Read_Primitive(stream, char, c);
        stringLength++;
    } while(c != 0);
    ret = (char*)__WZ_CurrentContext->calloc(stringLength, sizeof(char));
    mseek(stream, stringStart, SEEK_SET);
    mread(ret, sizeof(char), stringLength, stream);
    return ret;
}
char* M_Read_Chars(MEMFILE* stream, int count) {
    char* ret = (char*)__WZ_CurrentContext->calloc(count, sizeof(char));
    int i = 0;
    for(i = 0; i < count; i++) {
        char c;
        M_Read_Primitive(stream, char, c);
        ret[i] = c;
    }
    return ret;
}
char* M_Read_EncryptedString(MEMFILE* stream) {
    char smallLength = 0;
    char* ret = NULL;
    M_Read_Primitive(stream, signed char, smallLength);
    if(smallLength == 0) {
        ret = (char*)__WZ_CurrentContext->calloc(1, sizeof(char));
        ret[0] = '\0';
        return ret;
    }
    if(smallLength > 0) {
        unsigned short mask = 0xAAAA;
        unsigned int length = 0;
        unsigned int i = 0;
        if(smallLength == 0x7F)
            M_Read_Primitive(stream, signed int, length);
        else
            length = smallLength;
        if(length <= 0) {
            ret = (char*)__WZ_CurrentContext->calloc(1, sizeof(char));
            ret[0] = '\0';
            return ret;
        }
        ret = (char*)__WZ_CurrentContext->calloc(length + 1, sizeof(char)); /*1 extra for null//*/
        for(i = 0; i < length; i++) {
            unsigned short encChar;
            M_Read_Primitive(stream, unsigned short, encChar);
            ret[i] = (char)encChar;
            ret[i] ^= mask;
            ret[i] ^= (__WZ_CurrentContext->key[i * 2 + 1] << 8) + __WZ_CurrentContext->key[i * 2];
            mask++;
        }
        ret[length] = 0;
    } else {
        unsigned int mask = 0xAA;
        unsigned int length = 0;
        unsigned int i = 0;
        if(smallLength == -128)
            M_Read_Primitive(stream, unsigned int, length);
        else
            length = -smallLength;
        if(length <= 0)
            return "";
        ret = (char*)__WZ_CurrentContext->calloc(length + 1, sizeof(char));
        mread(ret, sizeof(char), length, stream);
        for(i = 0; i < length; i++) {
            ret[i] ^= mask;
            ret[i] ^= __WZ_CurrentContext->key[i];
            mask++;
        }
        ret[length] = 0;
    }
    return ret;
}
int M_Read_CompressedInt(MEMFILE* stream) {
    signed char chk;
    int val;
    M_Read_Primitive(stream, signed char, chk);
    if(chk != -128)
        return (int)chk;
    M_Read_Primitive(stream, int, val);
    return val;
}
unsigned int M_Read_Offset(MEMFILE* stream, WZ_Header* header) { /*I'll need a header for this*/
    unsigned int offset = mtell(stream);
    unsigned int xor;
    offset = (offset - header->fileStart) ^ 0xFFFFFFFF;
    offset *= header->versionHash;
    offset -= 0x581C3F6D;
    offset = (offset << (offset & 0x1f)) | (offset >> (32 - offset & 0x1F));
    M_Read_Primitive(stream, unsigned int, xor);
    offset ^= xor;
    offset += header->fileStart * 2;
    return offset;
}
char* M_Read_StringBlock(MEMFILE* stream, long offset) {
    char* ret;
    unsigned char b;
    long currentOffset = 0;
    int offinc;
    M_Read_Primitive(stream, unsigned char, b);
    switch(b) {
    case 0:
    case 0x73:
        ret = M_Read_EncryptedString(stream);
        break;
    case 1:
    case 0x1B:
        M_Read_Primitive(stream, int, offinc);
        offset += offinc;
        currentOffset = mtell(stream);
        mseek(stream, offset, SEEK_SET);
        ret = M_Read_EncryptedString(stream);
        mseek(stream, currentOffset, SEEK_SET);
        break;
    default:
        ret = (char*)__WZ_CurrentContext->calloc(1, sizeof(char));
        ret[0] = '\0';
        break;
    }
    return ret;
}
char* M_Read_StringAtOffset(MEMFILE* stream, unsigned int offset) {
    long cur = mtell(stream);
    char* ret = NULL;
    mseek(stream, offset, SEEK_SET);
    ret = M_Read_EncryptedString(stream);
    mseek(stream, cur, SEEK_SET);
    return ret;
}

#pragma endregion

#pragma region Error Functions
const char* WZ_GetError() {
    return __WZ_LastError;
}
WZ_ErrorCode    _WZ_Error_SetAndReturn  (WZ_ErrorCode err, char* format, ...) {
    va_list ap;
    const int maxLength = 512;
    char* msg = NULL;
    if(__WZ_LastError != NULL)
        __WZ_CurrentContext->free(__WZ_LastError);
    msg = (char*)__WZ_CurrentContext->calloc(maxLength, sizeof(char));
    va_start(ap, format);
    vsnprintf(msg, maxLength, format, ap);
    va_end(ap);
    __WZ_LastError = msg;
    return err;
}
char* WZ_GetError_copy() {
    int l = strlen(__WZ_LastError);
    char* ret = (char*)malloc(l + 1);
    memcpy(ret, __WZ_LastError, l + 1);
    return ret;
}
#pragma endregion

#pragma region Name Getters

char*       WZ_PropertyType_GetName(WZ_PropertyType propType) {
    switch(propType) {
    case WZ_PropertyType_Null:
        return "WZNullProperty";
    case WZ_PropertyType_UnsignedShort:
        return "WZUnsignedShortProperty";
    case WZ_PropertyType_CompressedInt:
        return "WZCompressedIntProperty";
    case WZ_PropertyType_Vector:
        return "WZVectorProperty";
    case WZ_PropertyType_Float:
        return "WZFloatProperty";
    case WZ_PropertyType_Double:
        return "WZDoubleProperty";
    case WZ_PropertyType_String:
        return "WZStringProperty";
    case WZ_PropertyType_UOL:
        return "WZUOLProperty";
    case WZ_PropertyType_Sub:
        return "WZSubProperty";
    case WZ_PropertyType_Canvas:
        return "WZCanvasProperty";
    case WZ_PropertyType_Convex:
        return "WZConvexProperty";
    case WZ_PropertyType_PNG:
        return "WZPNGProperty";
    case WZ_PropertyType_MP3:
        return "WZMP3Property";
    }
    return "WZUnknownProperty";
}
char*       WZ_ObjectType_GetName(WZ_ObjectType type) {
    switch(type) {
    case WZ_ObjectType_File:
        return "WZFile";
    case WZ_ObjectType_Directory:
        return "WZDirectory";
    case WZ_ObjectType_Image:
        return "WZImage";
    case WZ_ObjectType_Property:
        return "WZProperty";
    case WZ_ObjectType_Header:
        return "WZFileHeader";
    }
    return "Unknown";
}
#pragma endregion

#pragma region WZContext
WZ_Context* WZ_Context_New  () {
    WZ_Context* ret = NULL;
    ret = (WZ_Context*)__WZ_CurrentContext->calloc(1, sizeof(*ret));
    WZ_Context_SetDefaults(ret);
    return ret;
}
void WZ_Context_SetDefaults (WZ_Context* ctx) {
    memcpy((void*)ctx, (void*)&__WZ_DefaultContext, sizeof(*ctx));
}
WZ_ErrorCode    WZ_Context_LoadKeyFromFile  (WZ_Context* ctx, MEMFILE* stream) {
    unsigned char* buf = NULL;
    unsigned int length = 0;
    mseek(stream, 0, SEEK_END);
    length = mtell(stream);
    mseek(stream, 0, SEEK_SET);
    mread((void*)buf, sizeof(char), length, stream);
    return WZ_Context_SetKey(ctx, buf, length);
}
WZ_ErrorCode    WZ_Context_SetKey   (WZ_Context* ctx, unsigned char* buffer, unsigned int length) {
    if(ctx == NULL)
        return _WZ_Error_SetAndReturn(WZ_Error_NullParam, "ctx is NULL");
    if(buffer == NULL)
        return _WZ_Error_SetAndReturn(WZ_Error_NullParam, "buffer is NULL");
    if(length != 65536)
        return _WZ_Error_SetAndReturn(WZ_Error_Context_Key_BadLength, "length is not 65536");
    if(ctx->key != NULL) {
        if(ctx->free)
            ctx->free(ctx->key);
        else
            free(ctx->key);
    }
    ctx->key = (unsigned char*)(ctx->calloc ? ctx->calloc : calloc)(length, sizeof(char));
    memcpy((void*)ctx->key, (void*)buffer, sizeof(char)*length);
    return WZ_Error_NoError;
}
void WZ_Context_SetCurrent  (WZ_Context* ctx) {
    if(ctx != NULL) {
        __WZ_CurrentContext = ctx;
        if(!ctx->calloc)
            ctx->calloc = calloc;
        if(!ctx->realloc)
            ctx->realloc = realloc;
        if(!ctx->free)
            ctx->free = free;
        if(!ctx->key)
            ctx->key = __WZ_DefaultKey;
    } else
        __WZ_CurrentContext = &__WZ_DefaultContext;
}
WZ_Context* WZ_Context_GetCurrent   () {
    return __WZ_CurrentContext;
}
void    WZ_Context_Free (WZ_Context* ctx) {
    if(ctx == NULL)
        return;
    if(ctx->key != NULL)
        free(ctx->key);
    free(ctx);
}
#pragma endregion


#pragma region WZ_Object and Generics

WZ_ErrorCode    _WZ_Object_Init(WZ_Context* ctx, WZ_Object* obj, const char* name, WZ_ObjectType type) {
    obj->name = (char*)__WZ_CurrentContext->calloc((strlen(name) + 1), sizeof(char));
    memcpy(obj->name, name, strlen(name) + 1);
    obj->type = type;
    obj->context = ctx;
    obj->objectID = __WZ_OBJECT_ID_COUNTER;
    __WZ_OBJECT_ID_COUNTER++;
    return WZ_Error_NoError;
}
WZ_Object*  WZ_Object_Get_n(WZ_Object* obj, const char* name) {
    if(obj->type == WZ_ObjectType_Image)
        return (WZ_Object*)WZ_Image_Get_n((WZ_Image*)obj, name);
    if(obj->type == WZ_ObjectType_Directory)
        return WZ_Directory_Get_n((WZ_Directory*)obj, name);
    if(obj->type == WZ_ObjectType_File)
        return WZ_File_Get_n((WZ_File*)obj, name);
    if(obj->type == WZ_ObjectType_Property)
        if(WZ_SPROPTYPE(obj) == WZ_PropertyType_Sub)
            return (WZ_Object*)WZ_SubProperty_Get_n((WZ_SubProperty*)obj, name);
    _WZ_Error_SetAndReturn(WZ_Error_NoSuchChild, "could not find object");
    return NULL;
}
WZ_Object*  WZ_Object_Get_i(WZ_Object* obj, int index) {
    if(obj->type == WZ_ObjectType_Image)
        return (WZ_Object*)WZ_Image_Get_i((WZ_Image*)obj, index);
    if(obj->type == WZ_ObjectType_Directory)
        return WZ_Directory_Get_i((WZ_Directory*)obj, index);
    if(obj->type == WZ_ObjectType_File)
        return WZ_File_Get_i((WZ_File*)obj, index);
    if(obj->type == WZ_ObjectType_Property)
        if(WZ_SPROPTYPE(obj) == WZ_PropertyType_Sub)
            return (WZ_Object*)WZ_SubProperty_Get_i((WZ_SubProperty*)obj, index);
    _WZ_Error_SetAndReturn(WZ_Error_NoSuchChild, "could not find object");
    return NULL;
}
WZ_Object*  WZ_Object_Get_iv(WZ_Object* obj, ...) {
    va_list ap;
    WZ_Object* currentObj = obj;
    int ci;
    va_start(ap, obj);
    ci = va_arg(ap, int);
    while(ci != -1) {
        currentObj = WZ_Object_Get_i(currentObj, ci);
        if(currentObj == NULL)
            break;
        ci = va_arg(ap, int);
    }
    va_end(ap);
    if(currentObj == NULL)
        _WZ_Error_SetAndReturn(WZ_Error_BadChain, "could not follow chain");
    return currentObj;
}
WZ_Object*  WZ_Object_Get_nv(WZ_Object* obj, ...) {
    va_list ap;
    WZ_Object* currentObj = obj;
    char* ci;
    va_start(ap, obj);
    ci = va_arg(ap, char*);
    while(ci != NULL && strcmp(ci, "") != 0) {
        currentObj = WZ_Object_Get_n(currentObj, ci);
        if(currentObj == NULL)
            break;
        ci = va_arg(ap, char*);
    }
    va_end(ap);
    if(currentObj == NULL)
        _WZ_Error_SetAndReturn(WZ_Error_BadChain, "could not follow chain");
    return currentObj;
}
int     WZ_Object_GetSize(WZ_Object* obj) {
    if(obj->type == WZ_ObjectType_Image)
        return WZ_Image_GetSize((WZ_Image*)obj);
    if(obj->type == WZ_ObjectType_Directory)
        return WZ_Directory_GetSize((WZ_Directory*)obj);
    if(obj->type == WZ_ObjectType_File)
        return WZ_File_GetSize((WZ_File*)obj);
    if(obj->type == WZ_ObjectType_Property)
        if(WZ_SPROPTYPE(obj) == WZ_PropertyType_Sub)
            return WZ_SubProperty_GetSize((WZ_SubProperty*)obj);
    _WZ_Error_SetAndReturn(WZ_Error_BadOperation, "no size associated with this object");
    return -1;
}
#pragma endregion

#pragma region WZ_Image
WZ_ErrorCode _WZ_Image_Init(WZ_Context* ctx, WZ_Image* img, const char* name) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    memset(img, '\0', sizeof(WZ_Image));
    if((ret = _WZ_Object_Init(ctx, &(img->inh), name, WZ_ObjectType_Image)) != WZ_Error_NoError)
        return ret;
    img->_sub = (WZ_SubProperty*)(ctx->calloc ? ctx->calloc : calloc)(1, sizeof(WZ_SubProperty));
    if((ret = _WZ_SubProperty_Init(ctx, img->_sub, name)) != WZ_Error_NoError)
        return ret;
    img->_sub->inh.parentImg = img;
    WZ_OBJECT(img)->parent = (WZ_Object*)img;
    return ret;
}
void WZ_Image_Unparse(WZ_Image* image) {
    if(!WZ_OBJECT(image)->parsed)
        return;
    _WZ_SubProperty_Free(image->_sub);
    WZ_OBJECT(image)->parsed = 0;
}
WZ_ErrorCode    WZ_Image_Parse(WZ_Image* image) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    long originalPos;
    unsigned char b = 0;
    char* str;
    unsigned short c;
    if(image->inh.parsed)
        return WZ_Error_NoError;
    originalPos = mtell(image->_stream);
    mseek(image->_stream, image->inh.offset, SEEK_SET);
    M_Read_Primitive(image->_stream, unsigned char, b);
    if(b != 0x73)
        return WZ_Error_Image_Parse_Error1;
    str = M_Read_EncryptedString(image->_stream);
    if(strcmp(str, "Property") != 0) {
        __WZ_CurrentContext->free(str);
        return WZ_Error_Image_Parse_Error1;
    }
    __WZ_CurrentContext->free(str);
    M_Read_Primitive(image->_stream, unsigned short, c);
    if(c != 0)
        return WZ_Error_Image_Parse_Error1;
    image->_sub->_firstChild = NULL;
    image->_sub->inh.parentImg = image;
    WZ_OBJECT(image->_sub)->parent = (WZ_Object*)image;
    ret = _WZ_SubProperty_Parse(image->_sub, image->_stream, image->inh.offset);
    mseek(image->_stream, originalPos, SEEK_SET);
    if(ret == WZ_Error_NoError)
        image->inh.parsed = 1;
    return ret;
}
WZ_Property* WZ_Image_Get_n(WZ_Image* image, const char* name) {
    return WZ_SubProperty_Get_n(image->_sub, name);
}
WZ_Property* WZ_Image_Get_i(WZ_Image* image, int index) {
    return WZ_SubProperty_Get_i(image->_sub, index);
}
int WZ_Image_GetSize(WZ_Image* image) {
    return WZ_SubProperty_GetSize(image->_sub);
}
WZ_ErrorCode WZ_Image_ResolveUOLs(WZ_Image* image) {
    return WZ_SubProperty_ResolveUOLs(image->_sub);
}
#pragma endregion

#pragma region WZ_Property
WZ_ErrorCode _WZ_Property_Init(WZ_Context* ctx, WZ_Property* prop, const char* name, WZ_PropertyType propType) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    if((ret = _WZ_Object_Init(ctx, &(prop->inh), name, WZ_ObjectType_Property)) != WZ_Error_NoError)
        return ret;
    prop->propType = propType;
    return ret;
}
WZ_Property* WZ_Property_Copy(WZ_Property* prop) {
    WZ_Property* ret = NULL;
    char* name = WZ_OBJECT(prop)->name;
    /*primitive, sub, png, mp3 */
    switch(WZ_SPROPTYPE(prop)) {
    case WZ_PropertyType_Primitive: {
        ret = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_PrimitiveProperty));
        memcpy(WZPP(ret), WZPP(prop), sizeof(WZ_PrimitiveProperty));
        _WZ_PrimitiveProperty_Init(WZ_OBJECT(prop)->context, (WZ_PrimitiveProperty*)ret, name, prop->propType);
        switch(prop->propType) {
        case WZ_PropertyType_String:
        case WZ_PropertyType_UOL:
            /*alloc*/
            WZPP(ret)->val.strVal = (char*)__WZ_CurrentContext->calloc((strlen(WZPP(prop)->val.strVal) + 1), sizeof(char));
            memcpy(WZPP(ret)->val.strVal, WZPP(prop)->val.strVal, strlen(WZPP(prop)->val.strVal));
            break;
        case WZ_PropertyType_Vector:
            memcpy(WZPP(ret)->val.vecVal, WZPP(prop)->val.vecVal, 2 * sizeof(int));
            break;
        }
    }
    break;
    case WZ_PropertyType_Sub: {
        ret = WZ_PROPERTY(_WZ_SubProperty_Copy(WZ_SUBPROPERTY(prop)));
        switch(prop->propType) {
        case WZ_PropertyType_Canvas:
            ((WZ_CanvasProperty*)ret)->png = _WZ_PNGProperty_Copy(((WZ_PNGProperty*)WZ_CANVASPROPERTY(prop)->png));
            break;
        }
    }
    break;
    case WZ_PropertyType_MP3: {
        ret = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_SoundProperty));
        _WZ_SoundProperty_Init(WZ_OBJECT(prop)->context, (WZ_SoundProperty*)ret, name);
    }
    break;
    }
    return ret;
}
#pragma endregion

#pragma region WZ_PrimitiveProperty
WZ_ErrorCode _WZ_PrimitiveProperty_Init(WZ_Context* ctx, WZ_PrimitiveProperty* prop, const char* name, WZ_PropertyType propType) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    if((ret = _WZ_Property_Init(ctx, &(prop->inh), name, propType)) != WZ_Error_NoError)
        return ret;
    return ret;
}
#pragma endregion

#pragma region WZ_UOLProperty
WZ_Property* WZ_UOLProperty_Resolve(WZ_PrimitiveProperty* uolProp) {
    char** tokens;
    int numTokens;
    int i = 0;
    WZ_Property* curProp = WZ_PROPERTY(WZ_OBJECT(uolProp)->parent);
    if(WZ_PROPERTY(uolProp)->propType != WZ_PropertyType_UOL)
        return NULL;
    _WZ_tokenize(uolProp->val.strVal, "/", &tokens, &numTokens);
    for(i = 0; i < numTokens; i++) {
        if(strncmp(tokens[i], "..", 2) == 0) {
            WZ_Object* obj = WZ_OBJECT(curProp)->parent;
            if(obj == NULL) { /*oh teh noez*/
                curProp = NULL;
                break;
            }
            if(obj->type != WZ_ObjectType_Property) { /*oh teh noez*/
                curProp = NULL;
                break;
            }
            curProp = (WZ_Property*)obj;
            continue;
        }
        /*go down*/
        {
            WZ_Object* obj = WZ_Object_Get_n(WZ_OBJECT(curProp), tokens[i]);
            if(obj == NULL) { /*oh teh noez*/
                curProp = NULL;
                break;
            }
            curProp = (WZ_Property*)obj;
        }
    }
    for(i = 0; i < numTokens; i++)
        __WZ_CurrentContext->free(tokens[i]);
    __WZ_CurrentContext->free(tokens);
    return curProp;
}
#pragma endregion

#pragma region WZ_PNGProperty
WZ_ErrorCode _WZ_PNGProperty_Init(WZ_Context* ctx, WZ_PNGProperty* png, const char* name) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    if((ret = _WZ_Property_Init(ctx, (WZ_Property*)png, name, WZ_PropertyType_PNG)) != WZ_Error_NoError)
        return ret;
    return ret;
}
void WZ_PNGProperty_Unparse(WZ_PNGProperty* png) {
    if(png == NULL)
        return;
    if(!WZ_OBJECT(png)->parsed)
        return;
    if(!png->pngParsed)
        return;
    png->pngParsed = 0;
    if(png->_pixels != NULL) {
        __WZ_CurrentContext->free(png->_pixels);
        png->_pixels = NULL;
        png->_pixelsLen = 0;
    }
    if(WZ_OBJECT(png)->context->unparsePNG != NULL)
        WZ_OBJECT(png)->context->unparsePNG(WZ_OBJECT(png)->context, png, WZ_OBJECT(png)->context->unparsePNGUserData);
}
WZ_ErrorCode WZ_PNGProperty_Parse(WZ_PNGProperty* png) {
    unsigned char* decBuf = NULL;
    int decLen = 0;
    int f = png->_format + png->_format2;
    int h = png->height;
    int w = png->width;
    //h=(int)pow(2,ceil(log((float)h)/log(2.0f)));
    //w=(int)pow(2,ceil(log((float)w)/log(2.0f)));
    if(png->_pixels != NULL) {
        __WZ_CurrentContext->free(png->_pixels);
        png->_pixels = NULL;
        png->_pixelsLen = 0;
    }
    if(png->_decBytes == NULL) {
        switch(png->_format + png->_format2) {
        case 1:
        case 2:
            decLen = (32 / 8) * png->width * png->height;
            break;
        case 513:
            decLen = (16 / 8) * png->width * png->height;
            break;
        case 517:
            decLen = png->width * png->height / 128;
            break;
        }
        if(png->_compBytes[0] == 0x78 && png->_compBytes[1] == 0x9c)
            _WZ_z_decompress_known(png->_compBytes, png->_length, &decBuf, decLen);
        else {
            int blocksize = 0;
            int eop = png->_length;
            int loc = 0;
            unsigned char* tempCB = (unsigned char*)__WZ_CurrentContext->calloc(png->_length, sizeof(unsigned char));
            int tempCBLoc = 0;
            memset(tempCB, 0, sizeof(unsigned char)*png->_length);
            while(loc < (eop - 1)) {
                int i = 0;
                blocksize = 0;
                blocksize += (png->_compBytes[loc]  <<  0);
                blocksize += (png->_compBytes[loc + 1] <<  8);
                blocksize += (png->_compBytes[loc + 2] << 16);
                blocksize += (png->_compBytes[loc + 3] << 24);
                loc += 4;
                for(i = 0; i < blocksize; i++) {
                    tempCB[tempCBLoc] = png->_compBytes[loc] ^ __WZ_CurrentContext->key[i];
                    loc++;
                    tempCBLoc++;
                }
            }
            _WZ_z_decompress_known(tempCB, tempCBLoc++, &decBuf, decLen);
            __WZ_CurrentContext->free(tempCB);
        }
    }
    if(decLen == 0)
        decLen = png->_decLen;
    if(decBuf == NULL)
        decBuf = png->_decBytes;
    switch(f) {
    case 1: {
        int x, y, k;
        int r, g, b, a;
        png->_pixelsLen = w * h * 32 / sizeof(int);
        png->_pixels = (unsigned char*)__WZ_CurrentContext->calloc((png->_pixelsLen), sizeof(unsigned char));
        for(k = 0, x = 0, y = 0; k < decLen; k += 2) {
            if(x == png->width) {
                x = 0;
                y++;
                if(y == png->height)
                    break;
            }
            b = decBuf[k] & 15;
            b |= b << 4;
            g = decBuf[k] & 240;
            g |= g >> 4;
            r = decBuf[k + 1] & 15;
            r |= r << 4;
            a = decBuf[k + 1] & 240;
            a |= a >> 4;
            //or abgr?
            ((unsigned int*)png->_pixels)[y * w + x] = a << 24 | b << 16 | g << 8 | r;
            x++;
        }
    }
    break;
    case 2: {
        png->_pixelsLen = w * h * 32 / sizeof(int);
        png->_pixels = (unsigned char*)__WZ_CurrentContext->calloc((png->_pixelsLen), sizeof(unsigned char));
        memcpy(png->_pixels, decBuf, decLen);
    }
    break;
    case 513: {
        png->_pixelsLen = w * h * 32 / sizeof(int);
        png->_pixels = (unsigned char*)__WZ_CurrentContext->calloc((png->_pixelsLen), sizeof(unsigned char));
        memcpy(png->_pixels, decBuf, decLen);
    }
    break;
    case 517: {
        /*//Map.wz -> Back -> midForest -> back -> 0.png*/
        png->_pixelsLen = w * h * 32 / sizeof(int);
        png->_pixels = (unsigned char*)__WZ_CurrentContext->calloc((png->_pixelsLen), sizeof(unsigned char));
        {
            int x = 0;
            int y = 0;
            int i = 0;
            unsigned char j = 0;
            int k = 0;
            for(i = 0; i < decLen; i++) {
                for(j = 0; j < 8; j++) {
                    unsigned char iB = ((decBuf[i] & (0x01 << (7 - j))) >> (7 - j)) * 0xFF;
                    for(k = 0; k < 16; k++) {
                        if(x == png->width) {
                            x = 0;
                            y++;
                        }
                        *((unsigned int*)png->_pixels + y * 32 / 4 + x) = iB << 24 | iB << 16 | iB << 8 | iB; //*((unsigned int*)png->png->pixels+y*png->png->pitch/4+x);
                        x++;
                    }
                }
            }
        }
    }
    break;
    default:
        _WZ_Error_SetAndReturn(WZ_Error_PNGProp_Parse_BadFormat, "invalid PNG format");
        break;
    }
    WZ_OBJECT(png)->parsed = 1;
    png->pngParsed = 1;
    if(decBuf != NULL)
        png->_decBytes = decBuf;
    if(decLen != 0)
        png->_decLen = decLen;
    if(WZ_OBJECT(png)->context->renderPNG != NULL)
        WZ_OBJECT(png)->context->renderPNG(WZ_OBJECT(png)->context, png, WZ_OBJECT(png)->context->renderPNGUserData);
    return WZ_Error_NoError;
}

WZ_ErrorCode _WZ_PNGProperty_Read(WZ_PNGProperty* png, MEMFILE* stream) {
    png->_stream = stream;
    WZ_OBJECT(png)->offset = -1;
    if(stream != NULL) {
        int length = 0;
        png->width = M_Read_CompressedInt(stream);
        png->height = M_Read_CompressedInt(stream);
        png->_format = M_Read_CompressedInt(stream);
        M_Read_Primitive(stream, unsigned char, png->_format2);
        mseek(stream, 4, SEEK_CUR);
        M_Read_Primitive(stream, int, length);
        png->_length = length;
        mseek(stream, 1, SEEK_CUR);
        if(length > 0)
            png->_compBytes = (unsigned char*)__WZ_CurrentContext->calloc(length, sizeof(unsigned char));
        WZ_OBJECT(png)->offset = mtell(stream);
        if(length > 0)
            mread(png->_compBytes, sizeof(unsigned char), length, stream);
        WZ_OBJECT(png)->parsed = 1;
    }
    return WZ_Error_NoError;
}
WZ_PNGProperty* _WZ_PNGProperty_Copy(WZ_PNGProperty* pngProp) {
    WZ_PNGProperty* ret = (WZ_PNGProperty*)__WZ_CurrentContext->calloc(1, sizeof(WZ_PNGProperty));
    char* name = WZ_OBJECT(pngProp)->name;
    memcpy(ret, pngProp, sizeof(WZ_PNGProperty));
    _WZ_PNGProperty_Init(WZ_OBJECT(pngProp)->context, ret, name);
    if(pngProp->_compBytes != NULL) {
        ret->_compBytes = (unsigned char*)__WZ_CurrentContext->calloc(ret->_length, sizeof(unsigned char));
        memcpy(ret->_compBytes, pngProp->_compBytes, ret->_length);
    }
    if(pngProp->_decBytes != NULL) {
        ret->_decBytes = (unsigned char*)__WZ_CurrentContext->calloc(ret->_decLen, sizeof(unsigned char));
        memcpy(ret->_decBytes, pngProp->_decBytes, ret->_decLen);
    }
    return ret;
}
#pragma endregion

#pragma region WZ_CanvasProperty
WZ_ErrorCode _WZ_CanvasProperty_Init(WZ_Context* ctx, WZ_CanvasProperty* canvas, const char* name) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    if((ret = _WZ_SubProperty_Init(ctx, (WZ_SubProperty*)canvas, name)) != WZ_Error_NoError)
        return ret;
    WZ_PROPERTY(canvas)->propType = WZ_PropertyType_Canvas;
    return ret;
}
#pragma endregion

#pragma region WZ_SubProperty
WZ_ErrorCode _WZ_SubProperty_Init(WZ_Context* ctx, WZ_SubProperty* sub, const char* name) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    if((ret = _WZ_Property_Init(ctx, (WZ_Property*)sub, name, WZ_PropertyType_Sub)) != WZ_Error_NoError)
        return ret;
    return ret;
}
WZ_Property* WZ_SubProperty_Get_n(WZ_SubProperty* sub, const char* name) {
    EWF_LL* cur = sub->_firstChild;
    if(cur == NULL)
        return NULL;
    do {
        if(strcmp(WZ_OBJECT(cur->data)->name, name) == 0)
            return (WZ_Property*)cur->data;
        cur = cur->next;
    } while(cur != NULL);
    return NULL;
}
WZ_Property* WZ_SubProperty_Get_i(WZ_SubProperty* sub, int index) {
    EWF_LL* cur = sub->_firstChild;
    int curIndex = 0;
    if(cur == NULL)
        return NULL;
    do {
        if(curIndex == index)
            return (WZ_Property*)cur->data;
        curIndex++;
        cur = cur->next;
    } while(cur != NULL);
    return NULL;
}
int WZ_SubProperty_GetSize(WZ_SubProperty* sub) {
    return EWF_LL_GetSize(sub->_firstChild);
}
EWF_LL* _WZ_SubProperty_Add(WZ_SubProperty* sub, WZ_Property* prop) {
    EWF_LL* newE = EWF_LL_New((void*)prop);
    sub->_firstChild = EWF_LL_Attach(sub->_firstChild, newE);
    return newE;
}
void _WZ_SubProperty_Free(WZ_SubProperty* sub) {
    EWF_LL* cur = sub->_firstChild;
    while(cur != NULL) {
        WZ_Property* prop = WZ_PROPERTY(cur->data);
        if(prop->propType == WZ_PropertyType_Canvas) {
            WZ_PNGProperty_Unparse(WZ_CANVASPROPERTY(prop)->png);
            __WZ_CurrentContext->free(WZ_OBJECT(WZ_CANVASPROPERTY(prop)->png)->name);
            __WZ_CurrentContext->free(WZ_CANVASPROPERTY(prop)->png);
        }
        if(WZ_SPROPTYPE(prop) == WZ_PropertyType_Sub)
            _WZ_SubProperty_Free(WZ_SUBPROPERTY(prop));
        if(prop->propType == WZ_PropertyType_MP3)
            _WZ_SoundProperty_Free((WZ_SoundProperty*)prop);
        if(prop->propType == WZ_PropertyType_String || prop->propType == WZ_PropertyType_UOL)
            __WZ_CurrentContext->free(WZ_PRIMITIVEPROPERTY(prop)->val.strVal);
        __WZ_CurrentContext->free(WZ_OBJECT(prop)->name);
        /* now just kill it */
        __WZ_CurrentContext->free(cur->prev);
        if(cur->next == NULL) {
            __WZ_CurrentContext->free(cur);
            cur = NULL;
        } else
            cur = cur->next;
        __WZ_CurrentContext->free(prop);
    }
    __WZ_CurrentContext->free(cur);
    sub->_firstChild = NULL;
}
WZ_ErrorCode _WZ_SubProperty_Parse(WZ_SubProperty* sub, MEMFILE* stream, unsigned int offset) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    int entryCount = M_Read_CompressedInt(stream);
    int i = 0;
    for(i = 0; i < entryCount; i++) {
        char* name;
        unsigned char b = 0;
        WZ_Property* prop = NULL;
        name = M_Read_StringBlock(stream, offset);
        M_Read_Primitive(stream, unsigned char, b);
        switch(b) {
        case 0x00: {
            prop = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_PrimitiveProperty));
            _WZ_PrimitiveProperty_Init(WZ_OBJECT(sub)->context, (WZ_PrimitiveProperty*)prop, name, WZ_PropertyType_Null);
        }
        break;
        case 0x0B:
        case 0x02: {
            prop = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_PrimitiveProperty));
            _WZ_PrimitiveProperty_Init(WZ_OBJECT(sub)->context, (WZ_PrimitiveProperty*)prop, name, WZ_PropertyType_UnsignedShort);
            M_Read_Primitive(stream, unsigned short, WZPP(prop)->val.intVal);
        }
        break;
        case 0x03: {
            prop = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_PrimitiveProperty));
            _WZ_PrimitiveProperty_Init(WZ_OBJECT(sub)->context, (WZ_PrimitiveProperty*)prop, name, WZ_PropertyType_CompressedInt);
            WZPP(prop)->val.intVal = M_Read_CompressedInt(stream);
        }
        break;
        case 0x04: {
            unsigned char type = 0;
            M_Read_Primitive(stream, unsigned char, type);
            prop = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_PrimitiveProperty));
            _WZ_PrimitiveProperty_Init(WZ_OBJECT(sub)->context, (WZ_PrimitiveProperty*)prop, name, WZ_PropertyType_Float);
            if(type == 0x80)
                M_Read_Primitive(stream, float, WZPP(prop)->val.dblVal);
            else
                WZPP(prop)->val.dblVal = 0.0;
        }
        break;
        case 0x05: {
            prop = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_PrimitiveProperty));
            _WZ_PrimitiveProperty_Init(WZ_OBJECT(sub)->context, (WZ_PrimitiveProperty*)prop, name, WZ_PropertyType_Double);
            M_Read_Primitive(stream, double, WZPP(prop)->val.dblVal);
        }
        break;
        case 0x08: {
            prop = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_PrimitiveProperty));
            _WZ_PrimitiveProperty_Init(WZ_OBJECT(sub)->context, (WZ_PrimitiveProperty*)prop, name, WZ_PropertyType_String);
            WZPP(prop)->val.strVal = M_Read_StringBlock(stream, offset);
        }
        break;
        case 0x09: {
            unsigned int temp = 0;
            int eob = 0;
            M_Read_Primitive(stream, unsigned int, temp);
            eob = (int)(temp + mtell(stream));
            prop = _WZ_ExtendedProperty_Parse(WZ_OBJECT(sub)->context, stream, name, offset, eob, sub->inh.parentImg);
            if(mtell(stream) != eob)
                mseek(stream, eob, SEEK_SET);
        }
        break;
        }
        if(prop != NULL) {
            prop->parentImg = ((WZ_Property*)sub)->parentImg;
            if(prop->propType == WZ_PropertyType_Canvas)
                WZ_OBJECT(WZ_CANVASPROPERTY(prop)->png)->parent = (WZ_Object*)prop;
            WZ_OBJECT(prop)->parent = (WZ_Object*)sub;
            _WZ_SubProperty_Add(sub, prop);
        }
        __WZ_CurrentContext->free(name);
    }
    return ret;
}
WZ_ErrorCode WZ_SubProperty_ResolveUOLs(WZ_SubProperty* subProp) {
    EWF_LL* cur = subProp->_firstChild;
    while(cur != NULL) {
        WZ_Property* prop = WZ_PROPERTY(cur->data);
        if(WZ_SPROPTYPE(prop) == WZ_PropertyType_Sub)
            WZ_SubProperty_ResolveUOLs(WZ_SUBPROPERTY(prop));
        else if(prop->propType == WZ_PropertyType_UOL) {
            WZ_Property* newProp = WZ_UOLProperty_Resolve(WZPP(prop));
            if(newProp != NULL) {
                newProp = WZ_Property_Copy(newProp);
                cur->data = (void*)newProp; /*replace w/ ref to other prop*/
                /*reparent*/
                ((WZ_Object*)newProp)->parent = (WZ_Object*)subProp;
                __WZ_CurrentContext->free(prop); /*kill old UOL*/
            }
        }
        cur = cur->next;
    }
    return WZ_Error_NoError;
}
WZ_SubProperty* _WZ_SubProperty_Copy(WZ_SubProperty* subProp) {
    WZ_SubProperty* ret = NULL;
    EWF_LL* cur = subProp->_firstChild;
    switch(subProp->inh.propType) {
    case WZ_PropertyType_Sub:
        ret = (WZ_SubProperty*)__WZ_CurrentContext->calloc(1, sizeof(WZ_SubProperty));
        memcpy(ret, subProp, sizeof(WZ_SubProperty));
        _WZ_SubProperty_Init(WZ_OBJECT(subProp)->context, ret, WZ_OBJECT(subProp)->name);
        break;
    case WZ_PropertyType_Canvas:
        ret = (WZ_SubProperty*)__WZ_CurrentContext->calloc(1, sizeof(WZ_CanvasProperty));
        memcpy(ret, subProp, sizeof(WZ_CanvasProperty));
        _WZ_CanvasProperty_Init(WZ_OBJECT(subProp)->context, (WZ_CanvasProperty*)ret, WZ_OBJECT(subProp)->name);
        break;
    }
    ret->_firstChild = NULL;
    while(cur != NULL) {
        WZ_Property* copyProp = WZ_Property_Copy(WZ_PROPERTY(cur->data));
        EWF_LL* newE = EWF_LL_New((void*)copyProp);
        ret->_firstChild = EWF_LL_Attach(ret->_firstChild, newE);
        cur = cur->next;
    }
    return ret;
}
#pragma endregion

#pragma region WZ_ExtendedProperty
WZ_Property* _WZ_ExtendedProperty_Parse(WZ_Context* ctx, MEMFILE* stream, const char* name, int offset, int eob, WZ_Image* parentImg) {
    unsigned char b = 0;
    char* iname;
    WZ_Property* ret = NULL;
    M_Read_Primitive(stream, unsigned char, b);
    if(b == 0x1B) {
        int inc;
        int readPos = offset;
        long currentPos = 0;
        M_Read_Primitive(stream, int, inc);
        readPos += inc;
        currentPos = mtell(stream);
        mseek(stream, readPos, SEEK_SET);
        iname = M_Read_EncryptedString(stream);
        mseek(stream, currentPos, SEEK_SET);
    } else
        iname = M_Read_EncryptedString(stream);
    if(strcmp(iname, "Property") == 0) {
        ret = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_SubProperty));
        _WZ_SubProperty_Init(ctx, (WZ_SubProperty*)ret, name);
        WZ_PROPERTY(ret)->parentImg = parentImg;
        mseek(stream, 2, SEEK_CUR);
        _WZ_SubProperty_Parse((WZ_SubProperty*)ret, stream, offset);
    }
    if(strcmp(iname, "Canvas") == 0) {
        unsigned char b;
        ret = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_CanvasProperty));
        _WZ_CanvasProperty_Init(ctx, (WZ_CanvasProperty*)ret, name);
        mseek(stream, 1, SEEK_CUR);
        M_Read_Primitive(stream, unsigned char, b);
        if(b == 1) {
            mseek(stream, 2, SEEK_CUR);
            _WZ_SubProperty_Parse((WZ_SubProperty*)ret, stream, offset);
        }
        ((WZ_CanvasProperty*)ret)->png = (WZ_PNGProperty*)__WZ_CurrentContext->calloc(1, sizeof(WZ_PNGProperty));
        _WZ_PNGProperty_Init(ctx, WZ_CANVASPROPERTY(ret)->png, name);
        _WZ_PNGProperty_Read(((WZ_CanvasProperty*)ret)->png, stream);
        WZ_PROPERTY(((WZ_CanvasProperty*)ret)->png)->parentImg = parentImg;
    }
    if(strcmp(iname, "Shape2D#Vector2D") == 0) {
        ret = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_PrimitiveProperty));
        _WZ_PrimitiveProperty_Init(ctx, (WZ_PrimitiveProperty*)ret, name, WZ_PropertyType_Vector);
        WZPP(ret)->val.vecVal[0] = M_Read_CompressedInt(stream);
        WZPP(ret)->val.vecVal[1] = M_Read_CompressedInt(stream);
    }
    if(strcmp(iname, "Shape2D#Convex2D") == 0) {
        int ec = M_Read_CompressedInt(stream);
        int i = 0;
        ret = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_SubProperty));
        _WZ_SubProperty_Init(ctx, (WZ_SubProperty*)ret, name);
        WZ_PROPERTY(ret)->parentImg = parentImg;
        for(i = 0; i < ec; i++) {
            WZ_Property* nP = _WZ_ExtendedProperty_Parse(ctx, stream, name, offset, eob, parentImg);
            _WZ_SubProperty_Add((WZ_SubProperty*)ret, nP);
            WZ_OBJECT(nP)->parent = (WZ_Object*)ret;
        }
    }
    if(strcmp(iname, "Sound_DX8") == 0) {
        ret = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_SoundProperty));
        _WZ_SoundProperty_Init(ctx, (WZ_SoundProperty*)ret, name);
        _WZ_SoundProperty_Parse((WZ_SoundProperty*)ret, stream);
    }
    if(strcmp(iname, "UOL") == 0) {
        unsigned char b;
        unsigned int offInc;
        ret = (WZ_Property*)__WZ_CurrentContext->calloc(1, sizeof(WZ_PrimitiveProperty));
        _WZ_PrimitiveProperty_Init(ctx, (WZ_PrimitiveProperty*)ret, name, WZ_PropertyType_UOL);
        mseek(stream, 1, SEEK_CUR);
        M_Read_Primitive(stream, unsigned char, b);
        switch(b) {
        case 0:
            WZPP(ret)->val.strVal = M_Read_EncryptedString(stream);
            break;
        case 1:
            M_Read_Primitive(stream, unsigned int, offInc);
            WZPP(ret)->val.strVal = M_Read_StringAtOffset(stream, offset + offInc);
            break;
        }
    }
    WZ_PROPERTY(ret)->parentImg = parentImg;
    __WZ_CurrentContext->free(iname);
    return ret;
}
#pragma endregion

#pragma region WZ_Header
WZ_ErrorCode _WZ_Header_Init(WZ_Context* ctx, WZ_Header* header, const char* name) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    memset(header, '\0', sizeof(WZ_Header));
    if((ret = _WZ_Object_Init(ctx, (WZ_Object*)header, name, WZ_ObjectType_Header)) != WZ_Error_NoError)
        return ret;
    return ret;
}
WZ_ErrorCode        _WZ_Header_Parse(WZ_Header* header, MEMFILE* stream) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    long p = 0;
    header->ident = M_Read_Chars(stream, 4);
    M_Read_Primitive(stream, unsigned long long, header->fileSize);
    M_Read_Primitive(stream, unsigned int, header->fileStart);
    header->copyright = M_Read_NullString(stream);
    p = mtell(stream);
    mseek(stream, header->fileStart, SEEK_SET);
    M_Read_Primitive(stream, unsigned short, header->version);
    mseek(stream, p, SEEK_SET);
    header->inh.parsed = 1;
    return ret;
}

#pragma endregion

#pragma region WZ_File
void WZ_File_Close(WZ_File* file) {
    _WZ_Directory_Free(file->_wzDir);
    __WZ_CurrentContext->free(file->_wzDir);
    __WZ_CurrentContext->free(file->_header);
    __WZ_CurrentContext->free(file);
}
WZ_File*    WZ_File_Open(WZ_Context* ctx, MEMFILE* stream, const char* name) {
    WZ_File* ret = (WZ_File*)__WZ_CurrentContext->calloc(1, sizeof(WZ_File));
    ret->_fileVersion = -1;
    ret->_stream = stream;
    WZ_OBJECT(ret)->context = ctx;
    _WZ_Object_Init(ctx, (WZ_Object*)ret, name, WZ_ObjectType_File);
    ret->_fileName = (char*)__WZ_CurrentContext->calloc((strlen(name) + 1), sizeof(char));
    memcpy((void*)ret->_fileName, (void*)name, strlen(name) + 1);
    /*test to see if name is valid*/
    ret->_header = (WZ_Header*)__WZ_CurrentContext->calloc(1, sizeof(WZ_Header));
    _WZ_Header_Init(ctx, ret->_header, name);
    ret->_header->inh.parent = (WZ_Object*)ret;
    return ret;
}
unsigned int        _WZ_File_GetVersionHash(int encver, int realver) {
    char versionNumberStr[5];
    int versionHash = 0;
    int a, b, c, d;
    unsigned int ret = 0;
    int i = 0, l = 0;
    l = sprintf(versionNumberStr, "%d", realver);
    if(l <= 0)
        return -1;
    for(i = 0; i < l; i++)
        versionHash = (32 * versionHash) + (int)versionNumberStr[i] + 1;
    a = (versionHash >> 24) & 0xFF;
    b = (versionHash >> 16) & 0xFF;
    c = (versionHash >> 8) & 0xFF;
    d = (versionHash    ) & 0xFF;
    ret = 0xFF ^ a ^ b ^ c ^ d;
    if(ret == encver)
        return versionHash;
    return 0;
}
WZ_ErrorCode        WZ_File_Parse(WZ_File* file) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    unsigned short dump;
    file->_wzDir = (WZ_Directory*)__WZ_CurrentContext->calloc(1, sizeof(WZ_Directory));
    _WZ_Directory_Init(WZ_OBJECT(file)->context, file->_wzDir, file->_fileName);
    file->_wzDir->parentFile = file;
    if(file->_stream == NULL)
        return WZ_Error_File_Parse_BadFileName;
    /*now parse header*/
    _WZ_Header_Parse(file->_header, file->_stream);
    mseek(file->_stream, file->_header->fileStart, SEEK_SET);
    M_Read_Primitive(file->_stream, short, dump);
    /*now determine version*/
    if(file->_fileVersion == -1) {
        int j = 1;
        for(j = 1; j < 0x7F; j++) {
            unsigned int vh;
            file->_fileVersion = j;
            vh = _WZ_File_GetVersionHash(file->_header->version, file->_fileVersion);
            if(vh != 0)
                file->_header->versionHash = vh; /*assume the last collision is the valid one*/
        }
    }
    ret = _WZ_Directory_Parse(file->_wzDir, file->_stream, file->_header);
    if(ret == WZ_Error_NoError)
        file->inh.parsed = 1;
    file->inh.parsed = 1;
    return ret;
}
WZ_Object* WZ_File_Get_n(WZ_File* file, const char* name) {
    return WZ_Directory_Get_n(file->_wzDir, name);
}
WZ_Object* WZ_File_Get_i(WZ_File* file, int index) {
    return WZ_Directory_Get_i(file->_wzDir, index);
}
int WZ_File_GetSize(WZ_File* file) {
    return WZ_Directory_GetSize(file->_wzDir);
}
#pragma endregion

#pragma region WZ_SoundProperty
WZ_ErrorCode _WZ_SoundProperty_Init(WZ_Context* ctx, WZ_SoundProperty* prop, const char* name) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    if((ret = _WZ_Property_Init(ctx, (WZ_Property*)prop, name, WZ_PropertyType_MP3)) != WZ_Error_NoError)
        return ret;
    return ret;
}
void _WZ_SoundProperty_Parse(WZ_SoundProperty* prop, MEMFILE* stream) {
    mseek(stream, 1, SEEK_CUR);
    prop->_lenData = M_Read_CompressedInt(stream);
    prop->lenMS = M_Read_CompressedInt(stream);
    prop->_data = (unsigned char*)__WZ_CurrentContext->calloc(2 + prop->_lenData, sizeof(unsigned char));
    mread(prop->_data + 2, sizeof(unsigned char), prop->_lenData, stream);
    prop->_data[0] = 0xff;
    prop->_data[1] = 0xf0;
    prop->_lenData += 2;
    WZ_OBJECT(prop)->parsed = 1;
    if(WZ_OBJECT(prop)->context->renderMP3 != NULL)
        WZ_OBJECT(prop)->context->renderMP3(WZ_OBJECT(prop)->context, prop, WZ_OBJECT(prop)->context->renderMP3UserData);
    return;
}
void _WZ_SoundProperty_Free(WZ_SoundProperty* prop) {
    if(!WZ_OBJECT(prop)->parsed)
        return;
    if(prop->_data != NULL) {
        __WZ_CurrentContext->free(prop->_data);
        prop->_data = NULL;
    }
    WZ_OBJECT(prop)->parsed = 0;
    if(WZ_OBJECT(prop)->context->unparseMP3 != NULL)
        WZ_OBJECT(prop)->context->unparseMP3(WZ_OBJECT(prop)->context, prop, WZ_OBJECT(prop)->context->unparseMP3UserData);
}
#pragma endregion

#pragma region WZ_Directory
WZ_ErrorCode _WZ_Directory_Init(WZ_Context* ctx, WZ_Directory* dir, const char* name) {
    WZ_ErrorCode ret = WZ_Error_NoError;
    if((ret = _WZ_Object_Init(ctx, (WZ_Object*)dir, name, WZ_ObjectType_Directory)) != WZ_Error_NoError)
        return ret;
    return ret;
}
void _WZ_Directory_Free(WZ_Directory* dir) {
    EWF_LL* cur = dir->_firstChild;
    while(cur != NULL) {
        WZ_Object* obj = WZ_OBJECT(cur->data);
        if(obj->type == WZ_ObjectType_Directory)
            _WZ_Directory_Free((WZ_Directory*)cur->data);
        else
            WZ_Image_Unparse((WZ_Image*)cur->data);
        /* and kill it */
        __WZ_CurrentContext->free(obj);
        __WZ_CurrentContext->free(cur->prev);
        cur = cur->next;
    }
}
WZ_ErrorCode        _WZ_Directory_Parse(WZ_Directory* dir, MEMFILE* stream, WZ_Header* header) {
    int count = M_Read_CompressedInt(stream);
    int i = 0;
    for(i = 0; i < count; i++) {
        unsigned char type;
        char* name;
        int fsize;
        int checksum;
        unsigned int offset;
        M_Read_Primitive(stream, unsigned char, type);
        if(type == 1) {
            int unknown;
            M_Read_Primitive(stream, int, unknown);
            M_Read_Primitive(stream, short, unknown);
            M_Read_Offset(stream, header);
            continue;
        } else if(type == 2) {
            int stringOffset;
            long pos;
            M_Read_Primitive(stream, int, stringOffset);
            pos = mtell(stream);
            mseek(stream, header->fileStart + stringOffset, SEEK_SET);
            M_Read_Primitive(stream, unsigned char, type);
            name = M_Read_EncryptedString(stream);
            mseek(stream, pos, SEEK_SET);
        } else if(type == 3 || type == 4)
            name = M_Read_EncryptedString(stream);
        else {
            continue;
        }
        fsize = M_Read_CompressedInt(stream);
        checksum = M_Read_CompressedInt(stream);
        offset = M_Read_Offset(stream, header);
        if(type == 3) {
            WZ_Directory* newDir = (WZ_Directory*)__WZ_CurrentContext->calloc(1, sizeof(WZ_Directory));
            int co = 0;
            EWF_LL* newEntry = NULL;
            _WZ_Directory_Init(WZ_OBJECT(dir)->context, newDir, name);
            /*WZ_Directory* newDir=_WZ_Directory_New(name);*/
            newEntry = EWF_LL_New((void*)newDir);
            dir->_firstChild = EWF_LL_Attach(dir->_firstChild, newEntry);
            newDir->inh.parent = (WZ_Object*)dir;
            newDir->inh.blockSize = fsize;
            newDir->inh.checksum = checksum;
            newDir->inh.offset = offset;
            co = mtell(stream);
            mseek(stream, offset, SEEK_SET);
            newDir->parentFile = dir->parentFile;
            if(_WZ_Directory_Parse(newDir, stream, header) != WZ_Error_NoError)
                return WZ_Error_Directory_Parse_Error1;
            mseek(stream, co, SEEK_SET);
        } else {
            WZ_Image* newImg = (WZ_Image*)__WZ_CurrentContext->calloc(1, sizeof(WZ_Image));
            EWF_LL* newEntry = NULL;
            _WZ_Image_Init(WZ_OBJECT(dir)->context, newImg, name);
            /*WZ_Image* newImg=_WZ_Image_New(name);*/
            newEntry = EWF_LL_New((void*)newImg);
            dir->_firstChild = EWF_LL_Attach(dir->_firstChild, newEntry);
            newImg->inh.parent = (WZ_Object*)dir;
            newImg->inh.blockSize = fsize;
            newImg->inh.checksum = checksum;
            newImg->inh.offset = offset;
            newImg->_stream = stream;
            newImg->parentFile = dir->parentFile;
        }
        __WZ_CurrentContext->free(name);
    }
    dir->inh.parsed = 1;
    return WZ_Error_NoError;
}
WZ_Object* WZ_Directory_Get_i(WZ_Directory* dir, int index) {
    int curIndex = 0;
    EWF_LL* cur = dir->_firstChild;
    do {
        if(curIndex == index)
            return (WZ_Object*)cur->data;
        cur = cur->next;
        curIndex++;
    } while(cur != NULL);
    return NULL;
}
WZ_Object* WZ_Directory_Get_n(WZ_Directory* dir, const char* name) {
    EWF_LL* cur = dir->_firstChild;
    do {
        if(strcmp(WZ_OBJECT(cur->data)->name, name) == 0)
            return WZ_OBJECT(cur->data);
        cur = cur->next;
    } while(cur != NULL);
    return NULL;
}
int WZ_Directory_GetSize(WZ_Directory* dir) {
    return EWF_LL_GetSize(dir->_firstChild);
}
#pragma endregion
