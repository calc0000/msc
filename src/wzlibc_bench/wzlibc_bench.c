#include "wzlibc.h"
#include "SDL/SDL.h"
#include <stdio.h>

void loadImages(WZ_Object* obj){
	switch(obj->type){
	case WZ_ObjectType_Directory:
		{
			unsigned int i=0,l=WZ_Object_GetSize(obj);
			printf("l: %d\n",l);
			for(;i<l;i++)
				loadImages(WZ_Object_Get_i(obj,i));
		}
		break;
	case WZ_ObjectType_Image:
		WZ_Image_Parse((WZ_Image*)obj);
		break;
	}
}

int main(int argc,char* argv[]){
	unsigned int el=0;
	SDL_Init(SDL_INIT_TIMER);

	{
		WZ_Context* ctx=WZ_Context_New();
		WZ_File* f=WZ_File_Open(ctx,mopen("wz40b/Data.wz","rb"),"Data.wz");
		unsigned int t=0;
		t=SDL_GetTicks();
		WZ_File_Parse(f);
		loadImages((WZ_Object*)f->_wzDir);
		el=SDL_GetTicks()-t;
	}

	printf("Took %d ticks.\n",el);

	SDL_Quit();
	system("pause");
}